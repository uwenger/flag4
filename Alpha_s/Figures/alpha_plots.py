import numpy as np
import matplotlib.pyplot as plt
import Image
import os
from matplotlib import rcParams
rcParams['mathtext.fontset']= 'stixsans'
######################################################################
def mklogo():
 # generate logo
 if not os.path.isfile("./plots/FLAG_Logo.png"):
  print ""
  print "alpha_plots.py: I need to generate FLAG_Logo.png first. The easiest way"
  print "                to do this is to run a different plot script with"
  print ""
  exit()

 axicon = fig.add_axes([xpos,ypos,.13,.13],frameon=False,xticks=[],yticks=[])
 im  = Image.open('./plots/FLAG_Logo.png')
 im2 = np.array(im).astype(np.float) / 255
 axicon.imshow(im2)


######################################################################
# DATA
R4_0=np.array([[4.1323,0.2871], [4.7211,0.2212], [4.7779,0.216], [4.7455,0.2142], [4.8858,0.2436], [4.7347,0.2533], [4.887,0.2399], [5.1865,0.2161], [5.8104,0.1752], [4.7349,0.2534], [6.9001,0.1752], [7.9521,0.1381], [9.5773,0.1404], [10.1455,0.1286], [11.0554,0.1119], [12.4641,0.1212], [14.5345,0.0959]])
R4_1=np.array([[3.7283,0.3705], [3.8151,0.3945], [4.7369,0.2994], [4.8712,0.2888], [4.95,0.2828], [5.0281,0.277], [4.8531,0.2869], [4.7627,0.3304], [4.848,0.3235], [8.0785,0.1783], [4.7742,0.3478], [8.0577,0.2063], [9.5668,0.1706]])

R4Flag3_0=np.array([[2.4741,0.21907], [2.5928,0.19887], [2.4915,0.22122], [2.6004,0.20574], [2.5437,0.24801], [2.5918,0.24007], [2.5297,0.25528], [2.5925,0.24492], [2.9889,0.18971], [2.5783,0.25044], [3.6188,0.17706], [3.9883,0.15053], [3.6533,0.17706], [4.0265,0.1504], [5.1929,0.13707], [5.7558,0.11674], [6.3012,0.11081], [6.828,0.038239]])
R4Flag3_1=np.array([[2.595,0.28733], [2.8425,0.25394], [3.2369,0.21072], [2.5945,0.29083], [2.8694,0.2538], [3.2677,0.21058], [2.5637,0.32328], [2.5968,0.31884], [3.4045,0.2359], [4.0182,0.19389], [4.6136,0.16211]])

R68_0=np.array([[4.1323,0.2861], [4.7211,0.198], [4.7779,0.191], [4.7455,0.1898], [4.8858,0.2548], [4.7347,0.2677], [4.887,0.2493], [5.1865,0.2156], [5.8104,0.1569], [4.7349,0.268], [6.9001,0.1864], [7.9521,0.1296], [9.5773,0.1545], [10.1455,0.1353], [11.0554,0.1088], [12.4641,0.1372], [14.5345,0.0958]])
R68_1=np.array([[3.7283,0.405], [3.8151,0.4245], [4.7369,0.3436], [4.8712,0.3327], [4.95,0.3267], [5.0281,0.3203], [4.8531,0.3311], [4.7627,0.3632], [4.848,0.3577], [8.0785,0.2121], [4.7742,0.367], [8.0577,0.2501], [9.5668,0.2101]])
######################################################################
xpos=0.1205
ypos=0.855
Pxpos=45.10
Pypos=+250
fig=plt.figure()
ax=fig.add_subplot(111)
######################################################################
# R4Flag3
plt.plot(R4Flag3_0[:,0],R4Flag3_0[:,1],'rs',ms=10,mec='r')
plt.plot(R4Flag3_1[:,0],R4Flag3_1[:,1],'go',color='g',mew=2,mec='g',mfc='w',ms=10,linewidth=3)
plt.xlabel('$\\rm w_0 m_\\eta$',fontsize=22,labelpad=-1)
plt.ylabel('$\\rm \\alpha_{\\rm eff}$ for $\\rm R_4$',fontsize=22,fontname="sans-serif")
plt.yticks([0.1,0.2,0.3,0.4],fontsize=16,fontname="sans-serif") 	# your choice of x-ticks
plt.xticks([3,4,5,6,7],fontsize=16,fontname="sans-serif") 	# your choice of x-ticks
plt.ylim((0,0.45))
plt.xlim((2,7.5))
plt.minorticks_on()
mklogo()
plt.savefig('plots/R4Flag3.eps',format='eps',dpi=800)
plt.savefig('plots/R4Flag3.pdf',format='pdf',dpi=800)
plt.savefig('plots/R4Flag3.png',format='png',dpi=400)

######################################################################
fig.clf()
# R4
plt.plot(R4_0[:,0],R4_0[:,1],'rs',ms=10,mec='r')
plt.plot(R4_1[:,0],R4_1[:,1],'go',color='g',mew=2,mec='g',mfc='w',ms=10,linewidth=3)
plt.xlabel('$\\rm r_1 m_p$',fontsize=22,labelpad=-1)
plt.ylabel('$\\rm \\alpha_{\\rm eff}$ for $\\rm R_4$',fontsize=22,fontname="sans-serif")
plt.yticks([0.1,0.2,0.3,0.4],fontsize=16,fontname="sans-serif") 	# your choice of x-ticks
plt.xticks([4,6,8,10,12,14],fontsize=16,fontname="sans-serif") 	# your choice of x-ticks
plt.ylim((0,0.45))
plt.minorticks_on()
mklogo()
plt.savefig('plots/R4.eps',format='eps',dpi=800)
plt.savefig('plots/R4.pdf',format='pdf',dpi=800)
plt.savefig('plots/R4.png',format='png',dpi=400)

######################################################################
fig.clf()
# R4
plt.plot(R4_0[:,0],R68_0[:,1],'rs',ms=10,mec='r')
plt.plot(R4_1[:,0],R68_1[:,1],'go',color='g',mew=2,mec='g',mfc='w',ms=10,linewidth=3)
plt.xlabel('$\\rm r_1 m_p$',fontsize=22,labelpad=-1)
plt.ylabel('$\\rm \\alpha_{\\rm eff}$ for $\\rm R_6/R_8$',fontsize=22,fontname="sans-serif")
plt.yticks([0.1,0.2,0.3,0.4],fontsize=16,fontname="sans-serif") 	# your choice of x-ticks
plt.xticks([4,6,8,10,12,14,16],fontsize=16,fontname="sans-serif") 	# your choice of x-ticks
plt.ylim((0,0.45))
plt.minorticks_on()
mklogo()
plt.savefig('plots/R68.eps',format='eps',dpi=800)
plt.savefig('plots/R68.pdf',format='pdf',dpi=800)
plt.savefig('plots/R68.png',format='png',dpi=400)

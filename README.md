# README #

The central git repository is hosted at bitbucket.org:5069/flag.git 
(note that the repository used for FLAG-3 is out-of-date and no longer maintained). 

Compared to our old gitserver bitbucket offers many state-of-the-art 
collaboration features that makes life for the WGs and the EB easier.

### Access ###
In order to get started at least one WG member will get read access to 
the main repository. To this end please send your bitbucket user ID to 
juettner@soton.ac.uk such that the account can be affiliated with the
main repository (new bitbucket accounts can be registered under
https://bitbucket.org/account/signup/).

### Workflow ###
We follow the forking workflow explained also here.
Forking means that a user generates a copy of the FLAG repository in his
bitbucket work space. The user can pull and push from this repository
without touching the main FLAG repository.
Under Settings→User and group access please add Andreas Juettner (5069)
and Urs Wenger (uwenger) with read access.

Once a WG wishes to add their updated section to the main repository
they will bring their forked repository up-to-date with respect to the
main repository and then issue a pull request:

  - bring your local repository up to date:

    - `git remote add upstream git@bitbucket.org:5069/flag.git`
    - `git pull upstream master`
    
  - issue a pull request:
    
    - first commit your local changes (`git add <files>` and then `git commit` followed by `git push`)
    - log into the bitbucket site and select "Pull requests" on the left hand side and then click on "Create pull request"

The EB will receive a
notification, revise the suggested changes and eventually merge the WG
contribution into the main branch.

### FAQ: ###
1. I have created a fork but I only have read access.

    You may have uploaded your ssh-key as "deployment key" for a particular bitbucket project. 
    Remove this key in the admin area of this project, click on your avatar and add it under Bitbucket Settings -> SSH Keys".

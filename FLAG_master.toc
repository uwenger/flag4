\contentsline {section}{\numberline {1}Introduction}{7}{section.1}
\contentsline {subsection}{\numberline {1.1}FLAG composition, guidelines and rules}{11}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Citation policy}{12}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}General issues}{12}{subsection.1.3}
\contentsline {section}{\numberline {2}Quality criteria, averaging and error estimation}{16}{section.2}
\contentsline {subsection}{\numberline {2.1}Systematic errors and colour code}{16}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Systematic effects and rating criteria}{17}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Heavy-quark actions}{20}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Conventions for the figures}{22}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Averages and estimates}{22}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Averaging procedure and error analysis}{24}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Averaging --- generic case}{24}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Nested averaging}{25}{subsubsection.2.3.2}
\contentsline {section}{\numberline {3}Quark masses}{28}{section.3}
\contentsline {subsection}{\numberline {3.1}Masses of the light quarks}{30}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}The physical point and isospin symmetry}{30}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Ambiguities in the separation of strong and electromagnetic contributions}{32}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Inclusion of electromagnetic effects in lattice QCD simulations}{34}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Lattice determination of $m_s$ and $m_{ud}$}{35}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}Lattice determinations of $m_s/m_{ud}$}{39}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}Lattice determination of $m_u$ and $m_d$}{41}{subsubsection.3.1.6}
\contentsline {subsubsection}{\numberline {3.1.7}Estimates for $R$ and $Q$}{48}{subsubsection.3.1.7}
\contentsline {subsection}{\numberline {3.2}Charm quark mass}{49}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}$N_f = 2+1$ results}{51}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}$N_f = 2+1+1$ results}{51}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Lattice determinations of the ratio $m_c/m_s$}{53}{subsubsection.3.2.3}
\contentsline {subsection}{\numberline {3.3}Bottom-quark mass}{55}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}$N_f=2+1$}{55}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}$N_f=2+1+1$}{57}{subsubsection.3.3.2}
\contentsline {section}{\numberline {4}Leptonic and semileptonic kaon and pion decay and $|V_{ud}|$ and $|V_{us}|$}{59}{section.4}
\contentsline {subsection}{\numberline {4.1}Experimental information concerning $|V_{ud}|$, $|V_{us}|$, $f_+(0)$ and $ {f_{K^\pm }}/{f_{\pi ^\pm }}$}{59}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Lattice results for $f_+(0)$ and $f_{K^\pm }/f_{\pi ^\pm }$}{60}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Direct determination of $f_+(0)$ and $f_{K^\pm }/f_{\pi ^\pm }$}{63}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Tests of the Standard Model}{69}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Analysis within the Standard Model}{71}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Direct determination of $f_{K^\pm }$ and $f_{\pi ^\pm }$}{73}{subsection.4.6}
\contentsline {section}{\numberline {5}Low-energy constants}{77}{section.5}
\contentsline {subsection}{\numberline {5.1}Chiral perturbation theory }{77}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Quark-mass dependence of pseudoscalar masses and decay constants}{78}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Pion form factors and charge radii}{80}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Goldstone boson scattering in a finite volume}{82}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}Partially quenched and mixed action formulations}{84}{subsubsection.5.1.4}
\contentsline {subsubsection}{\numberline {5.1.5}Correlation functions in the $\epsilon $-regime}{85}{subsubsection.5.1.5}
\contentsline {subsubsection}{\numberline {5.1.6}Energy levels of the QCD Hamiltonian in a box and $\delta $-regime}{87}{subsubsection.5.1.6}
\contentsline {subsubsection}{\numberline {5.1.7}Other methods for the extraction of the low-energy constants}{87}{subsubsection.5.1.7}
\contentsline {subsection}{\numberline {5.2}Extraction of $SU(2)$ low-energy constants }{88}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Results for the LO $SU(2)$ LECs }{90}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Results for the NLO $SU(2)$ LECs }{95}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}Epilogue}{103}{subsubsection.5.2.3}
\contentsline {subsection}{\numberline {5.3}Extraction of $SU(3)$ low-energy constants }{105}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Results for the LO and NLO $SU(3)$ LECs}{105}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Epilogue}{105}{subsubsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.3}Outlook}{109}{subsubsection.5.3.3}
\contentsline {section}{\numberline {6}Kaon mixing}{111}{section.6}
\contentsline {subsection}{\numberline {6.1}Indirect CP violation and $\epsilon _{K}$ in the SM }{111}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Lattice computation of $B_{\rm {K}}$}{115}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Kaon BSM $B$ parameters}{118}{subsection.6.3}
\contentsline {section}{\numberline {7}$D$-meson decay constants and form factors}{126}{section.7}
\contentsline {subsection}{\numberline {7.1}Leptonic decay constants $f_D$ and $f_{D_s}$}{126}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Form factors for $D\to \pi \ell \nu $ and $D\to K \ell \nu $ semileptonic decays}{129}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Form factors for $\Lambda _c\to \Lambda \ell \nu $ semileptonic decays}{133}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}Determinations of $|V_{cd}|$ and $|V_{cs}|$ and test of second-row CKM unitarity}{134}{subsection.7.4}
\contentsline {section}{\numberline {8}$B$-meson decay constants, mixing parameters and form factors}{141}{section.8}
\contentsline {subsection}{\numberline {8.1}Leptonic decay constants $f_B$ and $f_{B_s}$}{142}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Neutral $B$-meson mixing matrix elements}{150}{subsection.8.2}
\contentsline {subsubsection}{\numberline {8.2.1}Error treatment for $B$ meson bag parameters}{155}{subsubsection.8.2.1}
\contentsline {subsection}{\numberline {8.3}Semileptonic form factors for $B$ decays to light flavours}{156}{subsection.8.3}
\contentsline {subsubsection}{\numberline {8.3.1}Form factors for $B\to \pi \ell \nu $}{158}{subsubsection.8.3.1}
\contentsline {subsubsection}{\numberline {8.3.2}Form factors for $B_s\to K\ell \nu $}{162}{subsubsection.8.3.2}
\contentsline {subsubsection}{\numberline {8.3.3}Form factors for rare and radiative $B$-semileptonic decays to light flavours}{164}{subsubsection.8.3.3}
\contentsline {subsection}{\numberline {8.4}Semileptonic form factors for $B_{(s)} \to D_{(s)} \ell \nu $ and $B \to D^* \ell \nu $}{167}{subsection.8.4}
\contentsline {subsubsection}{\numberline {8.4.1} $B_{(s)} \rightarrow D_{(s)}$ decays}{170}{subsubsection.8.4.1}
\contentsline {subsubsection}{\numberline {8.4.2}Ratios of $B\to D\ell \nu $ form factors}{174}{subsubsection.8.4.2}
\contentsline {subsubsection}{\numberline {8.4.3}$B \rightarrow D^*$ decays}{175}{subsubsection.8.4.3}
\contentsline {subsection}{\numberline {8.5}Semileptonic form factors for $B_c\to \eta _c\ell \nu $ and $B_c\to J/\psi \ell \nu $}{176}{subsection.8.5}
\contentsline {subsection}{\numberline {8.6}Semileptonic form factors for $\Lambda _b\to p\ell \nu $ and $\Lambda _b\to \Lambda _c\ell \nu $}{176}{subsection.8.6}
\contentsline {subsection}{\numberline {8.7}Determination of $|V_{ub}|$}{178}{subsection.8.7}
\contentsline {subsection}{\numberline {8.8}Determination of $|V_{cb}|$}{182}{subsection.8.8}
\contentsline {section}{\numberline {9}The strong coupling $\alpha _{\rm s}$}{187}{section.9}
\contentsline {subsection}{\numberline {9.1}Introduction}{187}{subsection.9.1}
\contentsline {subsubsection}{\numberline {9.1.1}Scheme and scale dependence of $\alpha _s$ and $\Lambda _{\rm QCD}$}{188}{subsubsection.9.1.1}
\contentsline {subsubsection}{\numberline {9.1.2}Overview of the review of $\alpha _s$}{189}{subsubsection.9.1.2}
\contentsline {subsubsection}{\numberline {9.1.3}Additions to the FLAG 13 report}{190}{subsubsection.9.1.3}
\contentsline {subsubsection}{\numberline {9.1.4}Additions to the FLAG 16 report}{190}{subsubsection.9.1.4}
\contentsline {subsection}{\numberline {9.2}General issues}{190}{subsection.9.2}
\contentsline {subsubsection}{\numberline {9.2.1}Discussion of criteria for computations entering the averages}{190}{subsubsection.9.2.1}
\contentsline {subsubsection}{\numberline {9.2.2}Physical scale}{194}{subsubsection.9.2.2}
\contentsline {subsubsection}{\numberline {9.2.3}Studies of truncation errors of perturbation theory}{195}{subsubsection.9.2.3}
\contentsline {subsection}{\numberline {9.3}$\alpha _s$ from Step Scaling Methods}{196}{subsection.9.3}
\contentsline {subsubsection}{\numberline {9.3.1}General considerations}{196}{subsubsection.9.3.1}
\contentsline {subsubsection}{\numberline {9.3.2}Discussion of computations}{198}{subsubsection.9.3.2}
\contentsline {subsection}{\numberline {9.4}$\alpha _s$ from the potential at short distances}{200}{subsection.9.4}
\contentsline {subsubsection}{\numberline {9.4.1}General considerations}{200}{subsubsection.9.4.1}
\contentsline {subsubsection}{\numberline {9.4.2}Discussion of computations}{201}{subsubsection.9.4.2}
\contentsline {subsection}{\numberline {9.5}$\alpha _s$ from the vacuum polarization at short distances}{204}{subsection.9.5}
\contentsline {subsubsection}{\numberline {9.5.1}General considerations}{204}{subsubsection.9.5.1}
\contentsline {subsubsection}{\numberline {9.5.2}Discussion of computations}{205}{subsubsection.9.5.2}
\contentsline {subsection}{\numberline {9.6}$\alpha _s$ from observables at the lattice spacing scale}{206}{subsection.9.6}
\contentsline {subsubsection}{\numberline {9.6.1}General considerations}{206}{subsubsection.9.6.1}
\contentsline {subsubsection}{\numberline {9.6.2}Continuum limit}{207}{subsubsection.9.6.2}
\contentsline {subsubsection}{\numberline {9.6.3}Discussion of computations}{208}{subsubsection.9.6.3}
\contentsline {subsection}{\numberline {9.7}$\alpha _s$ from current two-point functions}{211}{subsection.9.7}
\contentsline {subsubsection}{\numberline {9.7.1}General considerations}{211}{subsubsection.9.7.1}
\contentsline {subsubsection}{\numberline {9.7.2}Discussion of computations}{213}{subsubsection.9.7.2}
\contentsline {subsection}{\numberline {9.8}$\alpha _s$ from QCD vertices}{216}{subsection.9.8}
\contentsline {subsubsection}{\numberline {9.8.1}General considerations}{216}{subsubsection.9.8.1}
\contentsline {subsubsection}{\numberline {9.8.2}Discussion of computations}{217}{subsubsection.9.8.2}
\contentsline {subsection}{\numberline {9.9}$\alpha _s$ from the eigenvalue spectrum of the Dirac operator}{219}{subsection.9.9}
\contentsline {subsubsection}{\numberline {9.9.1}General considerations}{219}{subsubsection.9.9.1}
\contentsline {subsubsection}{\numberline {9.9.2}Discussion of computations}{220}{subsubsection.9.9.2}
\contentsline {subsection}{\numberline {9.10}Summary}{221}{subsection.9.10}
\contentsline {subsubsection}{\numberline {9.10.1}The present situation}{221}{subsubsection.9.10.1}
\contentsline {subsubsection}{\numberline {9.10.2}Our range for $\alpha _{\overline {\rm MS}}^{(5)}$}{224}{subsubsection.9.10.2}
\contentsline {subsubsection}{\numberline {9.10.3}Ranges for $[r_0 \Lambda ]^{(N_{\hspace {-0.08 em} f})}$ and $\Lambda _{\overline {\mathrm {MS}}}$}{226}{subsubsection.9.10.3}
\contentsline {subsubsection}{\numberline {9.10.4}Conclusions}{228}{subsubsection.9.10.4}
\contentsline {section}{\numberline {10}Nucleon matrix elements}{230}{section.10}
\contentsline {subsection}{\numberline {10.1}Isovector and flavor diagonal charges of the nucleon}{230}{subsection.10.1}
\contentsline {subsubsection}{\numberline {10.1.1}Technical aspects of the calculations of NME}{232}{subsubsection.10.1.1}
\contentsline {subsubsection}{\numberline {10.1.2}Controlling excited-state contamination}{233}{subsubsection.10.1.2}
\contentsline {subsubsection}{\numberline {10.1.3}Renormalization and Symanzik improvement of local currents}{236}{subsubsection.10.1.3}
\contentsline {subsubsection}{\numberline {10.1.4}Extrapolations in $a$, $M_\pi $ and $M_\pi L$}{238}{subsubsection.10.1.4}
\contentsline {subsection}{\numberline {10.2}Quality criteria for nucleon matrix elements and averaging procedure }{240}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Isovector charges}{241}{subsection.10.3}
\contentsline {subsubsection}{\numberline {10.3.1}Results for $g_A^{u-d}$}{242}{subsubsection.10.3.1}
\contentsline {subsubsection}{\numberline {10.3.2}Results for $g_S^{u-d}$}{245}{subsubsection.10.3.2}
\contentsline {subsubsection}{\numberline {10.3.3}Results for $g_T^{u-d}$}{246}{subsubsection.10.3.3}
\contentsline {subsection}{\numberline {10.4}Flavor Diagonal Charges}{248}{subsection.10.4}
\contentsline {subsubsection}{\numberline {10.4.1}Results for $g_A^{u,d,s}$}{250}{subsubsection.10.4.1}
\contentsline {subsubsection}{\numberline {10.4.2}Results for $g_S^{u,d,s}$ from direct calculations of the matrix elements}{251}{subsubsection.10.4.2}
\contentsline {subsubsection}{\numberline {10.4.3}Results for $g_S^{u,d,s}$ using the Feynman-Hellmann theorem}{252}{subsubsection.10.4.3}
\contentsline {subsubsection}{\numberline {10.4.4}Summary of Results for $g_S^{u,d}$}{254}{subsubsection.10.4.4}
\contentsline {subsubsection}{\numberline {10.4.5}Results for $g_T^{u,d,s}$}{255}{subsubsection.10.4.5}
\contentsline {section}{Acknowledgments}{256}{section*.2}
\contentsline {section}{\numberline {A}Glossary}{265}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Lattice actions}{265}{subsection.A.1}
\contentsline {subsubsection}{\numberline {A.1.1}Gauge actions }{265}{subsubsection.A.1.1}
\contentsline {subsubsection}{\numberline {A.1.2}Light-quark actions }{266}{subsubsection.A.1.2}
\contentsline {subsubsection}{\numberline {A.1.3}Heavy-quark actions}{271}{subsubsection.A.1.3}
\contentsline {subsection}{\numberline {A.2}Setting the scale }{281}{subsection.A.2}
\contentsline {subsection}{\numberline {A.3}Matching and running }{283}{subsection.A.3}
\contentsline {subsection}{\numberline {A.4}Chiral extrapolation}{285}{subsection.A.4}
\contentsline {subsection}{\numberline {A.5}Parameterizations of semileptonic form factors}{287}{subsection.A.5}
\contentsline {paragraph}{The vector form factor for $B\to \pi \ell \nu $}{287}{section*.3}
\contentsline {paragraph}{The scalar form factor for $B\to \pi \ell \nu $}{289}{section*.4}
\contentsline {paragraph}{Extension to other form factors}{290}{section*.5}
\contentsline {subsection}{\numberline {A.6}Summary of simulated lattice actions}{291}{subsection.A.6}
\contentsline {section}{\numberline {B}Notes}{297}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Notes to section \ref {sec:qmass} on quark masses}{297}{subsection.B.1}
\contentsline {subsection}{\numberline {B.2}Notes to section \ref {sec:vusvud} on $|V_{ud}|$ and $|V_{us}|$}{314}{subsection.B.2}
\contentsline {subsection}{\numberline {B.3}Notes to section \ref {sec:LECs} on Low-Energy Constants}{320}{subsection.B.3}
\contentsline {subsection}{\numberline {B.4}Notes to section \ref {sec:BK} on Kaon mixing}{330}{subsection.B.4}
\contentsline {subsubsection}{\numberline {B.4.1}Kaon $B$-parameter $B_K$}{330}{subsubsection.B.4.1}
\contentsline {subsubsection}{\numberline {B.4.2}Kaon BSM $B$-parameters}{332}{subsubsection.B.4.2}
\contentsline {subsection}{\numberline {B.5}Notes to section \ref {sec:DDecays} on $D$-meson decay constants and form factors}{334}{subsection.B.5}
\contentsline {subsubsection}{\numberline {B.5.1}Form factors for semileptonic decays of charmed hadrons}{347}{subsubsection.B.5.1}
\contentsline {subsection}{\numberline {B.6}Notes to section \ref {sec:BDecays} on $B$-meson decay constants, mixing parameters and form factors}{353}{subsection.B.6}
\contentsline {subsubsection}{\numberline {B.6.1}$B_{(s)}$-meson decay constants}{353}{subsubsection.B.6.1}
\contentsline {subsubsection}{\numberline {B.6.2}$B_{(s)}$-meson mixing matrix elements}{364}{subsubsection.B.6.2}
\contentsline {subsubsection}{\numberline {B.6.3}Form factors entering determinations of $|V_{ub}|$ ($B \to \pi l\nu $, $B_s \to K l\nu $, $\Lambda _b \to pl\nu $)}{371}{subsubsection.B.6.3}
\contentsline {subsubsection}{\numberline {B.6.4}Form factors for rare decays of beauty hadrons}{376}{subsubsection.B.6.4}
\contentsline {subsubsection}{\numberline {B.6.5}Form factors entering determinations of $|V_{cb}|$ ($B_{(s)} \to D_{(s)}^{(*)} l\nu $, $\Lambda _b \to \Lambda _c l\nu $) and $R(D_{(s)})$}{380}{subsubsection.B.6.5}
\contentsline {subsection}{\numberline {B.7}Notes to section \ref {sec:alpha_s} on the strong coupling $\alpha _{\rm s}$}{386}{subsection.B.7}
\contentsline {subsubsection}{\numberline {B.7.1}Renormalization scale and perturbative behaviour}{386}{subsubsection.B.7.1}
\contentsline {subsubsection}{\numberline {B.7.2}Continuum limit}{392}{subsubsection.B.7.2}
\contentsline {subsection}{\numberline {B.8}Notes to section \ref {sec:NME} on nucleon matrix elements}{397}{subsection.B.8}

% !TEX root = ../FLAG_master.tex
\subsection{Bottom-quark mass}
\label{s:bmass}

Now we review the lattice results for the $\overline{\rm MS}$-bottom-quark mass $\overline{m}_b$.  Related heavy-quark actions and observables have been discussed in the FLAG 13 and 17 reviews \cite{Aoki:2013ldr,Aoki:2016frl}, and descriptions
can be found in Sec.~\ref{app:HQactions}.  In Tab.~\ref{tab:mb} we collect results for
$\overline{m}_b(\overline{m}_b)$ obtained with $N_f =2+1$ and
$2+1+1$ quark flavors in the sea.  Available results for the quark-mass ratio $m_b / m_c$ are also reported.
After discussing the various resutls we evaluate the corresponding FLAG averages.

\input{qmass/qmass_tab_mb}

\subsubsection{$N_f=2+1$}

HPQCD 13B ~\cite{Lee:2013mla} extracts $\overline{m}_b$ from a lattice
determination of the $\Upsilon$ energy in NRQCD and the experimental
value of the meson mass. The latter quantities yield the pole mass
which is related to the $\overline{\rm MS}$ mass in 3-loop
perturbation theory. The MILC coarse (0.12 fm) and fine (0.09 fm)
Asqtad-2+1-flavour ensembles are employed in the calculation. The bare
light-(sea)-quark masses correspond to a single, relatively heavy,
pion mass of about 300 MeV. No estimate of the finite-volume error is
given. This result is not used in our average.

The value of $\overline{m}_b(\overline{m}_b)$ reported in HPQCD 10
\cite{McNeile:2010ji} is computed in a very similar fashion to the one
in HPQCD 14A described in the following section on 2+1+1 flavor results, except that MILC
2+1-flavour-Asqtad ensembles are used under HISQ valence
quarks. The lattice spacings of the ensembles range from 0.18 to 0.045
fm and pion masses down to about 165 MeV. In all, 22 ensembles were
fit simultaneously. An estimate of the finite-volume error based on
leading-order perturbation theory for the moment ratio is also
provided. Details of perturbation theory and renormalization
systematics are given in Sec.~\ref{s:curr}.

Maezawa 16 reports a new result for the $b$ quark mass since the last FLAG review. However as discussed in the charm quark section, this calculation does not satisfy the criteria to be used in the FLAG average. As in the previous review, we take the HPQCD 10 result as our average,
%FLAGRESULT BEGIN
% TAG      &mb&END
% REFS     &\cite{McNeile:2010ji}&END
% UNITS    & '[MeV]' &END
% NUMRESULTS & 1  &END
% FLAVOURs & 2+1  &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
&N_f= 2+1 :  &\FLAGAVBEGIN\overline{m}_b(\overline{m}_b)& = 4.164 (23) \FLAGAVEND&&\Ref ~\mbox{\cite{McNeile:2010ji}}, 
\end{align}
%FLAGRESULTFORMULA END
Since HPQCD quotes $\overline{m}_b(\overline{m}_b)$ using $N_f
= 5$ running, we used that value in the average. The corresponding 4-flavor RGI
average is
%FLAGRESULT BEGIN
% TAG      &mb&END
% REFS     &\cite{McNeile:2010ji}&END
% UNITS    & '[MeV]' & END
% NUMRESULTS & 1  &END
% FLAVOURs & 2+1  &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
&N_f= 2+1 :  &\FLAGAVBEGIN M_b^{\rm RGI} & = 6.874(38)_m(54)_\Lambda = 6.874(66)\FLAGAVEND&&\Ref ~\mbox{\cite{McNeile:2010ji}}.
\end{align}
%FLAGRESULTFORMULA END


\subsubsection{$N_f=2+1+1$}
Results have been published by HPQCD using NRQCD and HISQ-quark
actions (HPQCD 14B ~\cite{Colquhoun:2014ica} and HPQCD
14A~\cite{Chakraborty:2014aca}, respectively).  In both works the
$b$-quark mass is computed with the moments method, that is, from
Euclidean-time moments of two-point, heavy-heavy meson correlation
functions (see also Sec.~\ref{s:curr} for a description of the method).

In HPQCD 14B the $b$-quark mass is computed from ratios of the moments
$R_n$ of heavy current-current correlation functions, namely 
%
\be
\left[\frac{R_n r_{n-2}}{R_{n-2}r_n}\right]^{1/2} \frac{\bar{M}_{\rm
    kin}}{2 m_b} = \frac{\bar{M}_{\Upsilon,\eta_b}}{2 \bar m_b(\mu)} ~
,
      \label{eq:moments}
\ee 
%
where $r_n$ are the perturbative moments calculated at N$^3$LO,
$\bar{M}_{\rm kin}$ is the spin-averaged kinetic mass of the
heavy-heavy vector and pseudoscalar mesons and
$\bar{M}_{\Upsilon,\eta_b}$ is the experimental spin average of the
$\Upsilon$ and $\eta_b$ masses.  The average kinetic mass $\bar{M}_{\rm kin}$
is chosen since in the lattice calculation the splitting of the
$\Upsilon$ and $\eta_b$ states is inverted.  In Eq.~(\ref{eq:moments})
the bare mass $m_b$ appearing on the left hand side is tuned so that
the spin-averaged mass agrees with experiment, while the mass
$\overline{m}_b$ at the fixed scale $\mu = 4.18$ GeV is extrapolated
to the continuum limit using three HISQ (MILC) ensembles with $a
\approx$ 0.15, 0.12 and 0.09 fm and two pion masses, one of which is
the physical one. Their final result is
$\overline{m}_b(\mu = 4.18\, \gev) = 4.207(26)$ GeV, where the error is
from adding systematic uncertainties in quadrature only (statistical
errors are smaller than $0.1 \%$ and ignored). The errors arise from
renormalization, perturbation theory, lattice spacing, and NRQCD
systematics. The finite-volume uncertainty is not estimated, but at
the lowest pion mass they have $ m_\pi L \simeq 4$, which leads to the
tag \good\ .

In HPQCD 14A the quark mass is computed using a similar strategy as
above but with HISQ heavy quarks instead of NRQCD. The gauge-field
ensembles are the same as in HPQCD 14B above plus the one with $a =
0.06$ fm (four lattice spacings in all).
Since the physical $b$ quark mass in units of the lattice spacing is always greater than one in these calculations, fits to correlation functions are restricted to $am_h \le 0.8$, and a high degree polynomial in $a m_{\eta_{h}}$, the corresponding pseudoscalar mass, is used in the fits to remove the lattice spacing errors. Finally, to obtain the physical $b$ quark mass, the moments are extrapolated to $m_{\eta_b}$.
Bare heavy-quark masses are
tuned to their physical values using the $\eta_h$ mesons, and ratios
of ratios yield $m_h/m_c$. The $\overline{\rm MS}$-charm-quark mass
determined as described in Sec.~\ref{s:cmass} then gives $m_b$. The
moment ratios are expanded using the OPE, and the quark masses and
$\alpha_S$ are determined from fits of the lattice ratios to this
expansion. The fits are complicated: HPQCD uses cubic splines for
valence- and sea-mass dependence, with several knots, and many priors
for 21 ratios to fit 29 data points. Taking this fit at face value
results in a $\good$ rating for the continuum limit since they use
four lattice spacings down to 0.06 fm. See however the detailed
discussion of the continuum limit given in Sec.~\ref{s:curr} on
$\alpha_S$.

The third four-flavour result~\cite{Bussone:2016iua} is from the ETM Collaboration and updates their preliminary result
appearing in a conference proceedings~\cite{Bussone:2014cha}. The calculation is performed on a set of configurations
generated with twisted Wilson fermions with three lattice spacings in
the range 0.06 to 0.09 fm and with pion masses in the range 210 to 440
MeV. The $b$-quark mass is determined from a ratio of heavy-light
pseudoscalar meson masses designed to yield the quark pole mass in the
static limit. The pole mass is related to the $\overline{\rm MS}$ mass
through perturbation theory at N$^3$LO. The key idea is that by taking
ratios of ratios, the $b$-quark mass is accessible through fits to
heavy-light(strange)-meson correlation functions computed on the
lattice in the range $\sim 1-2\times m_c$ and the static limit, the
latter being exactly 1. By simulating below $\overline{m}_b$, taking
the continuum limit is easier. They find
$\overline{m}_b(\overline{m}_b) = 4.26(3)(10)$ GeV, where the first
error is statistical and the second systematic. The dominant errors
come from setting the lattice scale and fit systematics.

The next new result since FLAG 17 is from Gambino, {\it et al}~\cite{Gambino:2017vkx}. The authors use twisted mass fermion ensembles from the ETM collaboration and the ETM ratio method as in ETM 16. Three values of the lattice spacing are used, ranging from 0.062 to 0.089 fm. Several volumes are also used. The light quark masses produce pions with masses from 210 to 450 MeV. The main difference with ETM 16 is that the authors use the kinetic mass defined in the heavy quark expansion (HQE)  to extract the $b$-quark mass instead of the pole mass.

The final $b$-quark mass result is FNAL/MILC/TUM 18~\cite{Bazavov:2018omf}. The mass is extracted from the same fit and analysis that is described in the charm quark mass section. Note that relativistic HISQ quarks are used (almost) all the way up to the $b$ quark mass (0.9 $am_b$) on the finest two lattices, $a=0.03$ and 0.042 fm. The authors investigated the effect of leaving out the heaviest points from the fit, and the result did not noticeably change.

All of the above results enter our average. We note that here the updated ETM result is consistent with the average and a stretching factor on the error is not used. The average and error is dominated by the very precise FNAL/MILC/TUM 18 value.
%FLAGRESULT BEGIN
% TAG      &mb &END
% REFS     &\cite{Chakraborty:2014aca,Colquhoun:2014ica,Bussone:2016iua,Gambino:2017vkx,Bazavov:2018omf}&END
% UNITS    & '[MeV]' &   &END
% NUMRESULTS & 5  &END
% FLAVOURs & 2+1+1  &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
&N_f= 2+1+1 :&\FLAGAVBEGIN\overline{m}_b(\overline{m}_b)& = 4.198 (12)\FLAGAVEND&&\Refs~\mbox{\cite{Chakraborty:2014aca,Colquhoun:2014ica,Bussone:2016iua,Gambino:2017vkx,Bazavov:2018omf}}.
\end{align}
%FLAGRESULTFORMULA END
Since HPQCD quotes $\overline{m}_b(\overline{m}_b)$ using $N_f= 5$
running, we used that value in the average. We have included a 100\%
correlation on the statistical errors of ETM 16 and Gambino 17 since
the same ensembles are used in both. This translates to the following
RGI average
%FLAGRESULT BEGIN
% TAG      &mb &END
% REFS     &\cite{Chakraborty:2014aca,Colquhoun:2014ica,Bussone:2016iua,Gambino:2017vkx,Bazavov:2018omf}&END
% UNITS    & '[MeV]' &END
% NUMRESULTS & 5  &END
% FLAVOURs & 2+1+1  &END
%FLAGRESULT END
%FLAGRESULTFORMULA BEGIN
\begin{align}
&N_f= 2+1+1 :&\FLAGAVBEGIN M_b^{\rm RGI} & = 6.936(20)_m(54)_\Lambda = 6.936(57)\FLAGAVEND&&\Refs~\mbox{\cite{Chakraborty:2014aca,Colquhoun:2014ica,Bussone:2016iua,Gambino:2017vkx,Bazavov:2018omf}}.
\end{align}
%FLAGRESULTFORMULA END


All the results for $\overline{m}_b(\overline{m}_b)$ discussed above
are shown in Fig.~\ref{fig:mb} together with the FLAG averages
corresponding to $N_f=2+1$ and $2+1+1$ quark flavors.
\begin{figure}[!htb]
\begin{center}
\psfig{file=qmass/Figures/mb,width=11cm}
\end{center}
\vspace{-1cm}
\caption{ \label{fig:mb} The $b$ quark mass, $N_f =2+1$ and $2+1+1$. The updated PDG value from
  Ref.~\cite{Tanabashi:2018oca} is reported for comparison.}
\end{figure}

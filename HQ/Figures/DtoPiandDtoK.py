# FLAG plot for D->pi/K f_+(0)
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm f_+^{D\pi}(0)$"			# plot title
plotnamestring	= "DtoPiandDtoK"			# filename for plot
plotxticks	= ([[0.55,0.65, 0.75 ]])# locations for x-ticks
nminorticks	= 3			# number of intermediate minor ticks
					# 0 if none
FS =14

REMOVEWHITESPACE=0
SHIFT = -0.15

yaxisstrings	= [				# x,y-location for y-strings
		    [+0.47,9.0,"$\\rm N_f=2+1+1$"],
		    [+0.47,3.5,"$\\rm N_f=2+1$"],
		    [+0.47,-0.4,"$\\rm N_f=2$"]
		  ]
xlimits		= [0.52,1.00]			# plot's xlimits
logo		= 'upper left'			# either of 'upper left'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos   		= 0.75				# x-position for the data labels 
					        # move data labels of l.h.s side
						# plot to nirvana
tpos2		= 1.0				# x-position for the data labels
						# here a reasonable choice
REV    		= 0				# reverse order of data points
PRELIM 		= 0				# add preliminary tag
MULTIPLE	= 1				# we want two scatter plots in 
						# one figure
# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

################################################################################
# data for l.h.s. scatter plot
################################################################################

#-- Nf=4 --                      
datDtoPiNf4=[                         
[0.612,	0.035,0.035,0.035,0.035,  "ETM 17D"	,['s','g','g',0,tpos]]
]
 
#-- Nf=3 --
datDtoPiNf3=[
[0.64,	0.03,0.03,0.067,0.067,  "FNAL/MILC 04" 	   ,['s','w','r',0,tpos]] ,
[0.666,	0.021,0.021,0.029,0.029,  "HPQCD 11 / 10B" ,['s','g','g',0,tpos]] ,
[0.615,	0.031,0.031,0.045,0.036,  "JLQCD 17B"    ,['o','w','g',0,tpos]]
]

#-- Nf=2 --                      
datDtoPiNf2=[                         
[0.65,	0.06,0.06,0.086,0.085,  "ETM 11B"	,['o','w','g',0,tpos]]
]
 
################################################################################
# data for r.h.s. scatter plot
################################################################################

datDtoKNf4=[
[0.765, 0.031,0.031,0.031,0.031,  ""   ,['s','g','g',0,tpos]]
]
datDtoKNf3=[
[0.73,	0.03,0.03,0.076,0.076,  "" 	        ,['s','w','r',0,tpos]] ,
[0.747,	0.011,0.011,0.019,0.019,  ""		,['s','g','g',0,tpos]] ,
[0.698,	0.029,0.029,0.043,0.036,  "" 	        ,['o','w','g',0,tpos]]
] 
datDtoKNf2=[                          
[0.76,	0.05,0.05,0.071,0.071,  "" 		,['o','w','g',0,tpos]]
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.



# first define FLAGaverage for l.h.s. plot, the one for the r.h.s. one follows below
FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
[np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0],
[0.666,0.029,0.029,0.00,0.00,"FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[0.612,0.035,0.035,0.00,0.00,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datDtoPiNf2,datDtoPiNf3,datDtoPiNf4]

# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 


# this is still a bit ugly but I need some stuff from pyplot here
import matplotlib.pyplot as plt
fig=plt.figure( )
# do the l.h.s. plot
ax=fig.add_subplot(1,2,1)
ax.spines["right"].set_color('none')
ax.patch.set_facecolor('None')
fig.subplots_adjust(wspace=0.)
execfile('FLAGplot.py')


# now some definitions for the r.h.s. plot
plotnamestring	= "DtoPiandDtoK"			# filename for plot

titlestring	= "$\\rm  f_+^{DK}(0)$"			# plot title
xlimits		= [0.50,0.87]			# plot's xlimits
plotxticks	= ([[0.65,0.75,0.85]])# locations for x-ticks

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> grey area bounded by solid black lines
	      # 1 -> dashed lines marking the edge of the interval
[np.nan,0.000,0.000,0.000,0.000,"placeholderxxxxxxxxxxxxxxxx$",['s','k','k',0,tpos],0],
[0.747,0.019,0.019,0.00,0.00,"",['s','k','k',0,tpos],0],
[0.765,0.031,0.031,0.00,0.00,"",['s','k','k',0,tpos],0]
]
yaxisstrings    = [                             # x,y-location for y-strings                                   
                    [+0.47,9.0,""],
                    [+0.47,3.5,""],
                    [+0.47,-0.4,""]
                  ]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datDtoKNf2,datDtoKNf3,datDtoKNf4]

logo		= 'none'
# do the r.h.s. plot
ax=fig.add_subplot(1,2,2)
ax.patch.set_facecolor('None')
ax.spines["left"].set_color('none')
execfile('FLAGplot.py')

################################################################################
# We are done
################################################################################


# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker


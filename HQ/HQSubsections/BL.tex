\subsection{Leptonic decay constants $f_B$ and $f_{B_s}$}
\label{sec:fB}
%
The $B$ and $B_s$ meson decay constants are crucial inputs for
extracting information from leptonic $B$ decays.  Charged $B$ mesons
can decay to the lepton-neutrino final state  through the
charged-current weak interaction.  On the other hand, neutral
$B_{d(s)}$ mesons can decay to a charged-lepton pair via a
flavour-changing neutral current (FCNC) process.



In the Standard Model the decay rate for $B^+ \to \ell^+ \nu_{\ell}$
is described by a formula identical to Eq.~(\ref{eq:Dtoellnu}), with $D_{(s)}$ replaced by $B$, and the 
relevant CKM matrix element, $V_{cq}$, replaced by $V_{ub}$,
\be
\Gamma ( B \to \ell \nu_{\ell} ) =  \frac{ m_B}{8 \pi} G_F^2  f_B^2 |V_{ub}|^2 m_{\ell}^2 
           \left(1-\frac{ m_{\ell}^2}{m_B^2} \right)^2 \;. \label{eq:B_leptonic_rate}
\ee
The only charged-current $B$ meson decay that has been observed so far is 
$B^{+} \to \tau^{+} \nu_{\tau}$, which has been measured by the Belle
and Babar collaborations~\cite{Lees:2012ju,Kronenbitter:2015kls}.
Both collaborations have reported results with errors around $20\%$. These measurements can be used to 
determine $|V_{ub}|$ when combined with lattice-QCD predictions of the corresponding
decay constant. 


Neutral $B_{d(s)}$-meson decays to a charged lepton pair, $B_{d(s)}
\rightarrow l^{+} l^{-}$ is a FCNC process, and can only occur at
%proceeds via a flavour-changing neutral current (FCNC) interaction, which 
one loop in the Standard Model.  Hence these proceses are expected to
be rare, and are sensitive to physics beyond the Standard Model.
The corresponding expression for the branching fraction has the form 
\be
B ( B_q \to \ell^+ \ell^-) = \tau_{B_q} \frac{G_F^2}{\pi} \, Y \,
\left(  \frac{\alpha}{4 \pi \sin^2 \Theta_W} \right)^2
m_{B_q} f_{B_q}^2 |V_{tb}^*V_{tq}|^2 m_{\ell}^2 
           \sqrt{1- 4 \frac{ m_{\ell}^2}{m_B^2} }\;, 
\ee
where the light quark $q=s$ or $d$, and the function $Y$ includes NLO QCD and electro-weak
corrections \cite{Inami:1980fz,Buchalla:1993bv}. Evidence for both
 $B_s \to \mu^+ \mu^-$ and $B_s \to \mu^+ \mu^-$ decays was first observed
by the CMS and the LHCb collaborations, and a combined analysis was
presented in 2014 in Ref.~\cite{CMS:2014xfa}.  In 2017,  the LHCb
collaboration reported their latest measurements as~\cite{Aaij:2017vad}
%
\begin{eqnarray} 
   B(B_d \to \mu^+ \mu^-) &=& (1.5^{+1.2+0.2}_{-1.0-0.1}) \,10^{-10} , \nonumber\\
   B(B_s \to \mu^+ \mu^-) &=& (3.0\pm 0.6^{+0.3}_{-0.2}) \,10^{-9} ,
\label{eq:B_to_mumu_CMS_LHCb_2014}
\end{eqnarray}
%
which are compatible with the Standard Model predictions~\cite{Bobeth:2013uxa}.


The decay constants $f_{B_q}$ (with $q=u,d,s$) parameterise the matrix
elements of the corresponding axial-vector currents, $A^{\mu}_{bq}
= \bar{b}\gamma^{\mu}\gamma^5q$, analogously to the definition of
$f_{D_q}$ in section~\ref{sec:fD}:
\be
\langle 0| A^{\mu} | B_q(p) \rangle = i p_B^{\mu} f_{B_q} \;.
\label{eq:fB_from_ME}
\ee
For heavy-light mesons, it is convenient to define and analyse the quantity 
\be
 \Phi_{B_q} \equiv f_{B_q} \sqrt{m_{B_q}} \;,
\ee
which approaches a constant (up to logarithmic corrections) in the
$m_B \to \infty$ limit because of the heavy-quark symmetry.
In the following discussion we denote lattice data for $\Phi$($f$)
obtained at a heavy quark mass $m_h$ and light valence-quark mass
$m_{\ell}$ as $\Phi_{h\ell}$($f_{hl}$), to differentiate them from
the corresponding quantities at the physical $b$ and light-quark
masses.


The $SU(3)$ breaking ratio, $f_{B_s}/f_B$, is of phenomenological
interest.  This is because in lattice-QCD calculations for this
quantity,  many systematic effects can be partially reduced.  
These include discretisation errors, heavy-quark mass
tuning effects, and renormalisation/matching errors, amongst others. 
On the other hand, 
this $SU(3)$ breaking ratio is still sensitive to the chiral
extrapolation.   Given that the chiral extrapolation is under control,
one can then adopt $f_{B_s}/f_B$ as input in extracting
phenomenologically-interesting quantities.  In addition, it often
happens to be easier to obtain lattice results for $f_{B_{s}}$ with
smaller errors.  Therefore, one can combine the $B_{s}$-meson
decay constant with the $SU(3)$ breaking ratio to calculate $f_{B}$.  Such
a strategy can lead to better precision in the computation of the
$B$-meson decay constant, and has been adopted by the
ETM~\cite{Carrasco:2013zta, Bussone:2016iua} and the
HPQCD collaborations~\cite{Na:2012sp}.  


It is clear that the decay constants for charged and neutral $B$
mesons play different roles in flavour physics phenomenology.  As
already mentioned above, the knowledge of the $B^{+}$-meson decay constant,
$f_{B^{+}}$, is essential for extracting $|V_{ub}|$ from
leptonic $B^{+}$ decays.   The neutral $B$-meson decay constants,
$f_{B^{0}}$ and $f_{B_{s}}$, are inputs for the search of new physics in rare leptonic $B^{0}$
decays.  In
view of this, it is desirable to include isospin breaking effects in
lattice computations for these quantities, and have results for
$f_{B^{+}}$ and $f_{B^{0}}$.   
%
With the increasing precision of recent lattice calculations, isospin-splittings for $B{-}$meson decays constants are significant, 
%
and will play an important role in the foreseeable
future.    A few collaborations reported $f_{B^{+}}$ and $f_{B^{0}}$
separately by taking into account strong isospin effects in the
valence sector, and estimated the corrections from
electromagnetism.   To properly use these results for extracting
phenomenologically relevant information, one would have to take into
account QED effects in the $B{-}$meson leptonic decay rates.\footnote{See Ref.~\cite{Carrasco:2015xwa} for a strategy that
has been proposed to account for QED effects.}  Currently, errors on the
experimental measurements on these decay rates are still very large.   In this review, we
will then concentrate on the isospin-averaged result, $f_{B}$, and the
$B_{s}$-meson decay constant, as well as the $SU(3)$ breaking ratio,
$f_{B_{s}}/f_{B}$.   For the world average for lattice
determination of $f_{B^{+}}$ and $f_{B_{s}}/f_{B^{+}}$,
we refer the reader to the latest work from the Particle Data
Group (PDG)~\cite{Rosner:2015wva}.   Notice that the $N_{f} = 2+1$ lattice results used in
Ref.~\cite{Rosner:2015wva} and the current review are identical.  We
will discuss this in further detail at the end of this subsection.


The status of lattice-QCD computations for $B$-meson decay constants
and the $SU(3)$ breaking ratio, using gauge-field ensembles
with light dynamical fermions, is summarised in Tables~\ref{tab:FBssumm}
and~\ref{tab:FBratsumm}.  Figures~\ref{fig:fB} and~\ref{fig:fBratio} contain the graphical
presentation of the collected results and our averages.  Many results
in these tables and plots were
already reviewed in detail in the previous FLAG
report.  Below we will describe the new results
that appeared after January 2016.  
%\begin{table}[htb]
\begin{table}[!htb]
\mbox{} \\[3.0cm]
\footnotesize
\begin{tabular*}{\textwidth}[l]{@{\extracolsep{\fill}}l@{\hspace{1mm}}r@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{5mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l}
Collaboration & Ref. & $\Nf$ & 
\hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{chiral extrapolation}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{finite volume}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{renormalisation/matching}\end{rotate}\hspace{-0.15cm}  &
\hspace{0.15cm}\begin{rotate}{60}{heavy quark treatment}\end{rotate}\hspace{-0.15cm} & 
 $f_{B^+}$ & $f_{B^0}$   & $f_{B}$ & $f_{B_s}$  \\
%\rule{0.12cm}{0cm} \parbox[b]{1.0cm}
&&&&&&&&&&&&\\[-0.1cm]
\hline
\hline
&&&&&&&&&&&& \\[-0.1cm]

FNAL/MILC 17  & \cite{Bazavov:2017lyh} & 2+1+1 & \gA & \good & \good & \good 
& \good &  \okay &  189.4(1.4) & 190.5(1.3) & 189.9(1.4) & 230.7(1.2) \\[0.5ex]

HPQCD 17A & \cite{Hughes:2017spc} & 2+1+1 & \gA & \good & \good & \good 
& \soso &  \okay &  $-$ & $-$ & 196(6) & 236(7) \\[0.5ex]

ETM 16B & \cite{Bussone:2016iua} & 2+1+1 & \gA & \good & \soso & \soso 
& \soso &  \okay &  $-$ & $-$ & 193(6) & 229(5) \\[0.5ex]

ETM 13E & \cite{Carrasco:2013naa} & 2+1+1 & \rC & \good & \soso & \soso 
& \soso &  \okay &  $-$ & $-$ & 196(9) & 235(9) \\[0.5ex]

HPQCD 13 & \cite{Dowdall:2013tga} & 2+1+1 & \gA & \good & \good & \good & \soso
& \okay &  184(4) & 188(4) &186(4) & 224(5)  \\[0.5ex]

&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]

RBC/UKQCD 14 & \cite{Christ:2014uea} & 2+1 & \gA & \soso & \soso & \soso 
  & \soso & \okay & 195.6(14.9) & 199.5(12.6) & $-$ & 235.4(12.2) \\[0.5ex]

RBC/UKQCD 14A & \cite{Aoki:2014nga} & 2+1 & \gA & \soso & \soso & \soso 
  & \soso & \okay & $-$ & $-$ & 219(31) & 264(37) \\[0.5ex]

RBC/UKQCD 13A & \cite{Witzel:2013sla} & 2+1 & \rC & \soso & \soso & \soso 
  & \soso & \okay & $-$ & $-$ &  191(6)$_{\rm stat}^\diamond$ & 233(5)$_{\rm stat}^\diamond$ \\[0.5ex]

HPQCD 12 & \cite{Na:2012sp} & 2+1 & \gA & \soso & \soso & \soso & \soso
& \okay & $-$ & $-$ & 191(9) & 228(10)  \\[0.5ex]

HPQCD 12 & \cite{Na:2012sp} & 2+1 & \gA & \soso & \soso & \soso & \soso
& \okay & $-$ & $-$ & 189(4)$^\triangle$ &  $-$  \\[0.5ex]

HPQCD 11A & \cite{McNeile:2011ng} & 2+1 & \gA & \good & \soso &
 \good & \good & \okay & $-$ & $-$ & $-$ & 225(4)$^\nabla$ \\[0.5ex] 

FNAL/MILC 11 & \cite{Bazavov:2011aa} & 2+1 & \gA & \soso & \soso &
     \good & \soso & \okay & 197(9) & $-$ & $-$ & 242(10) &  \\[0.5ex]  

HPQCD 09 & \cite{Gamiz:2009ku} & 2+1 & \gA & \soso & \soso & \soso &
\soso & \okay & $-$ & $-$ & 190(13)$^\bullet$ & 231(15)$^\bullet$  \\[0.5ex] 

&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]

ALPHA 14 & \cite{Bernardoni:2014fva} & 2 & \gA & \good & \good &\good 
& \good & \okay &  $-$ & $-$ & 186(13) & 224(14) \\[0.5ex]

ALPHA 13 & \cite{Bernardoni:2013oda} & 2 & \rC  & \good   & \good   &
\good    &\good  & \okay   & $-$ & $-$ & 187(12)(2) &  224(13) &  \\[0.5ex] 

ETM 13B, 13C$^\dagger$ & \cite{Carrasco:2013zta,Carrasco:2013iba} & 2 & \gA & \good & \soso & \good
& \soso &  \okay &  $-$ & $-$ & 189(8) & 228(8) \\[0.5ex]

ALPHA 12A& \cite{Bernardoni:2012ti} & 2 & \rC  & \good      & \good      &
\good          &\good  & \okay   & $-$ & $-$ & 193(9)(4) &  219(12) &  \\[0.5ex] 

ETM 12B & \cite{Carrasco:2012de} & 2 & \rC & \good & \soso & \good
& \soso &  \okay &  $-$ & $-$ & 197(10) & 234(6) \\[0.5ex]

ALPHA 11& \cite{Blossier:2011dk} & 2 & \rC  & \good      & \soso      &
\good          &\good  & \okay  & $-$ & $-$ & 174(11)(2) &  $-$ &  \\[0.5ex]  

ETM 11A & \cite{Dimopoulos:2011gx} & 2 & \gA & \soso & \soso & \good
& \soso &  \okay & $-$ & $-$ & 195(12) & 232(10) \\[0.5ex]

ETM 09D & \cite{Blossier:2009hg} & 2 & \gA & \soso & \soso & \soso
& \soso &  \okay & $-$ & $-$ & 194(16) & 235(12) \\[0.5ex]
&&&&&&&&&& \\[-0.1cm]
\hline
\hline
\end{tabular*}
%
%
\begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}lllllllll}
  \multicolumn{10}{l}{\vbox{\begin{flushleft} 
	$^\diamond$Statistical errors only. \\
        $^\triangle$Obtained by combining $f_{B_s}$ from HPQCD 11A with $f_{B_s}/f_B$ calculated in this work.\\
        $^\nabla$This result uses one ensemble per lattice spacing with light to strange sea-quark mass 
        ratio $m_{\ell}/m_s \approx 0.2$. \\
        $^\bullet$This result uses an old determination of  $r_1=0.321(5)$ fm from Ref.~\cite{Gray:2005ur} that 
        has since been superseded. \\
        $^\dagger$Update of ETM 11A and 12B. 
\end{flushleft}}}
\end{tabular*}
\vspace{-0.5cm}
\caption{Decay constants of the $B$, $B^+$, $B^0$ and $B_{s}$ mesons
  (in MeV). Here $f_B$ stands for the mean value of $f_{B^+}$ and
  $f_{B^0}$, extrapolated (or interpolated) in the mass of the light
  valence-quark to the physical value of $m_{ud}$.}
\label{tab:FBssumm}
\end{table}

%
%
%
%
%
%
\begin{table}[!htb]
%\begin{table}[htb]
\begin{center}
\mbox{} \\[3.0cm]
\footnotesize
\begin{tabular*}{\textwidth}[l]{@{\extracolsep{\fill}}l@{\hspace{1mm}}r@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{5mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l@{\hspace{1mm}}l}
Collaboration & Ref. & $\Nf$ & 
\hspace{0.15cm}\begin{rotate}{60}{publication status}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{continuum extrapolation}\end{rotate}\hspace{-0.15cm} &
\hspace{0.15cm}\begin{rotate}{60}{chiral extrapolation}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{finite volume}\end{rotate}\hspace{-0.15cm}&
\hspace{0.15cm}\begin{rotate}{60}{renormalisation/matching}\end{rotate}\hspace{-0.15cm}  &
\hspace{0.15cm}\begin{rotate}{60}{heavy quark treatment}\end{rotate}\hspace{-0.15cm} & 
 $f_{B_s}/f_{B^+}$  & $f_{B_s}/f_{B^0}$  & $f_{B_s}/f_{B}$  \\
 %\rule{0.12cm}{0cm}
&&&&&&&&&& \\[-0.1cm]
\hline
\hline
&&&&&&&&&& \\[-0.1cm]

FNAL/MILC 17  & \cite{Bazavov:2017lyh} & 2+1+1 & \gA & \good & \good
                                                                                   & \good 
& \good &  \okay &  1.2180(49) & 1.2109(41) & $-$ \\[0.5ex]

HPQCD 17A & \cite{Hughes:2017spc} & 2+1+1 & \gA & \good & \good & \good 
& \soso &  \okay &  $-$ & $-$ & 1.207(7) \\[0.5ex]

ETM 16B & \cite{Bussone:2016iua} & 2+1+1 & \gA & \good & \soso & \soso 
& \soso &  \okay &  $-$ & $-$& 1.184(25) \\[0.5ex]

ETM 13E & \cite{Carrasco:2013naa} & 2+1+1 & \rC & \good & \soso & \soso
& \soso &  \okay &  $-$ & $-$ & 1.201(25) \\[0.5ex]

HPQCD 13 & \cite{Dowdall:2013tga} & 2+1+1 & \gA & \good & \good & \good & \soso
& \okay & 1.217(8) & 1.194(7) & 1.205(7)  \\[0.5ex]

&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]

RBC/UKQCD 14 & \cite{Christ:2014uea} & 2+1 & \gA & \soso & \soso & \soso 
  & \soso & \okay & 1.223(71) & 1.197(50) & $-$ \\[0.5ex]

RBC/UKQCD 14A & \cite{Aoki:2014nga} & 2+1 & \gA & \soso & \soso & \soso 
  & \soso & \okay & $-$ & $-$ & 1.193(48) \\[0.5ex]


RBC/UKQCD 13A & \cite{Witzel:2013sla} & 2+1 & \rC & \soso & \soso & \soso 
  & \soso & \okay & $-$ & $-$ &  1.20(2)$_{\rm stat}^\diamond$ \\[0.5ex]

HPQCD 12 & \cite{Na:2012sp} & 2+1 & \gA & \soso & \soso & \soso & \soso
& \okay & $-$ & $-$ & 1.188(18) \\[0.5ex]

FNAL/MILC 11 & \cite{Bazavov:2011aa} & 2+1 & \gA & \soso & \soso &
     \good& \soso & \okay & 1.229(26) & $-$ & $-$ \\[0.5ex]  
     
RBC/UKQCD 10C & \cite{Albertus:2010nm} & 2+1 & \gA & \tbr & \tbr & \tbr 
  & \soso & \okay & $-$ & $-$ & 1.15(12) \\[0.5ex]

HPQCD 09 & \cite{Gamiz:2009ku} & 2+1 & \gA & \soso & \soso & \soso &
\soso & \okay & $-$ & $-$ & 1.226(26)  \\[0.5ex] 

&&&&&&&&&& \\[-0.1cm]
\hline
&&&&&&&&&& \\[-0.1cm]
ALPHA 14 \al \cite{Bernardoni:2014fva} & 2 & \gA & \good & \good & \good 
& \good &  \okay &  $-$ \al $-$ & 1.203(65)\\[0.5ex]

ALPHA 13 & \cite{Bernardoni:2013oda} & 2 & \rC  & \good  & \good  &
\good   &\good  & \okay   & $-$ & $-$ & 1.195(61)(20)  &  \\[0.5ex] 

ETM 13B, 13C$^\dagger$ & \cite{Carrasco:2013zta,Carrasco:2013iba} & 2 & \gA & \good & \soso & \good
& \soso &  \okay &  $-$ & $-$ & 1.206(24)  \\[0.5ex]

ALPHA 12A & \cite{Bernardoni:2012ti} & 2 & \rC & \good & \good & \good
& \good &  \okay & $-$ & $-$ & 1.13(6)  \\ [0.5ex]

ETM 12B & \cite{Carrasco:2012de} & 2 & \rC & \good & \soso & \good
& \soso &  \okay & $-$ & $-$ & 1.19(5) \\ [0.5ex]

ETM 11A & \cite{Dimopoulos:2011gx} & 2 & \gA & \soso & \soso & \good
& \soso &  \okay & $-$ & $-$ & 1.19(5) \\ [0.5ex]
&&&&&&&&&& \\[-0.1cm]
\hline
\hline
\end{tabular*}
\begin{tabular*}{\textwidth}[l]{l@{\extracolsep{\fill}}lllllllll}
  \multicolumn{10}{l}{\vbox{\begin{flushleft}
 	 $^\diamond$Statistical errors only. \\
          $^\dagger$Update of ETM 11A and 12B. 
\end{flushleft}}}
\end{tabular*}
\vspace{-0.5cm}
\caption{Ratios of decay constants of the $B$ and $B_s$ mesons (for details see Table \ref{tab:FBssumm}).}
\label{tab:FBratsumm}
\end{center}
\end{table}
%
\begin{figure}[!htb]
%\begin{figure}[htb]
\centering	
%\hspace{-0.8cm}
\includegraphics[width=0.48\linewidth]{HQ/Figures/fB}
%\hspace{-0.95cm}
\includegraphics[width=0.48\linewidth]{HQ/Figures/fBs}
 \vspace{-2mm}
\caption{Decay constants of the $B$ and $B_s$ mesons. The values are taken from Table \ref{tab:FBssumm} 
(the $f_B$ entry for FNAL/MILC 11 represents $f_{B^+}$). The
significance of the colours is explained in
section~2.
%\ref{sec:qualcrit}
The black squares and grey bands indicate
our averages in Eqs.~(\ref{eq:fB2}), (\ref{eq:fB21}),
(\ref{eq:fB211}), (\ref{eq:fBs2}), (\ref{eq:fBs21}) and
(\ref{eq:fBs211}).}
\label{fig:fB}
\end{figure}
%
%
\begin{figure}[!htb]
%\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.58\linewidth]{HQ/Figures/fBratio}
\vspace{-2mm}
\caption{Ratio of the decay constants of the $B$ and $B_s$ mesons. The
  values are taken from Table \ref{tab:FBratsumm}.  Results labelled
  as FNAL/MILC 2017 1 and FNAL/MILC 2017 2 correspond to
  those for $f_{B_{s}}/f_{B^{0}}$ and $f_{B_{s}}/f_{B^{+}}$ reported in FNAL/MILC
  2017 .  The
significance of the colours is explained in
section~2.
%\ref{sec:qualcrit}. 
The black squares and grey bands indicate
our averages in Eqs.~(\ref{eq:fBratio2}), (\ref{eq:fBratio21}) and
(\ref{eq:fBratio211}).}
\label{fig:fBratio}
\end{center}
\end{figure}
%


%%%%%%%% N_f = 2 
No new $N_{f}=2$ and $N_{f}=2+1$  project for computing $f_{B}$, $f_{B_{s}}$ and
$f_{B_{s}}/f_{B}$ were completed after the publication of the previous
FLAG review~\cite{Aoki:2016frl}.  Therefore, our averages for these cases stay the same as
those in Ref.~\cite{Aoki:2016frl}. 
%
\begin{align}
&\label{eq:fB2}
\Nf=2:&\FLAGAVBEGIN f_{B} &= 188(7) \FLAGAVEND\;{\rm MeV}
&&\Refs~\mbox{\cite{Carrasco:2013zta,Bernardoni:2014fva}},\\
&\label{eq:fBs2}
\Nf=2: &\FLAGAVBEGIN f_{B_{s}} &= 227(7)\FLAGAVEND \; {\rm MeV} 
&&\Refs~\mbox{\cite{Carrasco:2013zta,Bernardoni:2014fva}}, \\
&\label{eq:fBratio2}
%\label{eq:fbav2}
\Nf=2: &\FLAGAVBEGIN f_{B_{s}}\over{f_B} &= 1.206(0.023)\FLAGAVEND
&&\Refs~\mbox{\cite{Carrasco:2013zta,Bernardoni:2014fva}},
\end{align}
%%%%%%% N_f = 2+1
%
\begin{align}
&\label{eq:fB21}
\Nf=2+1:&\FLAGAVBEGIN f_{B} &= 192.0(4.3) \FLAGAVEND\;{\rm MeV}
&&\Refs~\mbox{\cite{Bazavov:2011aa,McNeile:2011ng,Na:2012sp,Aoki:2014nga,Christ:2014uea}},\\
&\label{eq:fBs21}
\Nf=2+1: &\FLAGAVBEGIN f_{B_{s}} &= 228.4(3.7)\FLAGAVEND \; {\rm MeV} 
&&\Refs~\mbox{\cite{Bazavov:2011aa,McNeile:2011ng,Na:2012sp,Aoki:2014nga,Christ:2014uea}}, \\
&\label{eq:fBratio21}
%\label{eq:fbav21}
\Nf=2+1: &\FLAGAVBEGIN f_{B_{s}}\over{f_B} &= 1.201(0.016)\FLAGAVEND
&&\Refs~\mbox{\cite{Bazavov:2011aa,Na:2012sp,Aoki:2014nga,Christ:2014uea}},
\end{align}


%%%%%%% N_f = 2+1+1
There have been results for $f_{B_{(s)}}$ and $f_{B_{s}}/f_{B}$ from
three collaborations, ETMC, HPQCD and FNAL/MILC since the last FLAG
report.   In Tables~\ref{tab:FBssumm}
and \ref{tab:FBratsumm}, these results are labelled ETM
16B~\cite{Bussone:2016iua}, 
HPQCD 17A~\cite{Hughes:2017spc} and FNAL/MILC 17~\cite{Bazavov:2017lyh}.  


% beginning of the description of ETM 16B
In ETM 16B~\cite{Bussone:2016iua}, simulations at three values of lattice spacing,
$a=0.0885$, 0.0815 and 0.0619 fm are performed with twisted-mass
Wilson fermions and the Iwasaki gauge action.   The three lattice
spacings correspond to the bare couplings $\beta=1.90$, 1.95 and
2.10.  The pion masses in this work range from 210 to 450 MeV, and the
lattice sizes are between 1.97 and 2.98 fm.  An essential feature in
ETM 16B~\cite{Bussone:2016iua} is the use of the ratio method~\cite{Blossier:2009hg}.  In
the application of this approach to the $B$ decay constants,  one
first computes the quantity ${\mathcal{F}}_{hq} \equiv f_{hq}/M_{hq}$,
where $f_{hq}$ and $M_{hq}$ are decay constant and mass of the
pseudoscalar meson composed of valence (relativistic) heavy quark $h$
and light (or strange) quark $q$.   The matching between the lattice
and the continuum heavy-light currents for extracting the above
$f_{hq}$ is straightforward because 
the valence heavy quark is also described by twisted-mass fermions.  In the second step,
the ratio $z_{q} (\bar{\mu}^{(h)}, \lambda) \equiv
[{\mathcal{F}}_{hq}C_{A}^{{\mathrm{stat}}}(\bar{\mu}^{(h^{\prime})})(\mu_{{\mathrm{pole}}}^{(h)})^{3/2}]/[{\mathcal{F}}_{h^{\prime}q}C_{A}^{{\mathrm{stat}}}(\bar{\mu}^{(h)})(\mu_{{\mathrm{pole}}}^{(h^{\prime})})^{3/2}]$
is calculated, where $\mu_{{\mathrm{pole}}}^{(h)}$ is the pole mass of
the heavy quark $h$ with $\bar{\mu}^{(h)}$ being the corresponding
renormalised mass in a scheme (chosen to be the $\overline{{\mathrm{MS}}}$ scheme
in ETM 16B~\cite{Bussone:2016iua}),
$C_{A}^{{\mathrm{stat}}}(\bar{\mu}^{(h)})$ is the matching coefficient for the $(hq)$
meson decay constant in QCD and its counterpart in HQET, and $\bar{\mu}^{(h)}
= \lambda \bar{\mu}^{(h^{\prime})}$ with $\lambda$ being larger than, but close to,
one.  The authors of ETM 16B~\cite{Bussone:2016iua} use the NNLO perturbative result of
$C_{A}^{{\mathrm{stat}}}(\bar{\mu}^{(h)})$ in their work.   Notice
that in practice one never has to determine the heavy-quark pole mass
in this strategy, since it can be matched to the $\overline{{\mathrm{MS}}}$ mass,
and the matching coefficient is known to
NNNLO~\cite{Chetyrkin:1999pq,Chetyrkin:1999ys,Gray:1990yh}.    By
starting from a ``triggering'' point with the heavy-quark mass around that
of the charm, one can proceed with the calculations in steps, such that
$\bar{\mu}^{(h)}$ is increased by a factor of $\lambda$ at each step.
In ETM 16B~\cite{Bussone:2016iua} , the authors went up to heavy-quark mass
around 3.3 GeV, and observed that all systematics were under control.
In this approach, it is also crucial to employ the information that
$z_{q} (\bar{\mu}^{(h)}, \lambda) \rightarrow 1$ in the limit
$\bar{\mu}^{(h)} \rightarrow \infty$.   Designing the computations in
such a way that in the last step, $\bar{\mu}^{(h)}$ is equal to the pre-determined
bottom-quark mass in the same renormalisation scheme, one obtains
$f_{B_{(s)}}/M_{B_{(s)}}$.   Employing experimental results for
$M_{B_{(s)}}$, the decay constants can be extracted.   In ETM 16B~\cite{Bussone:2016iua},
this strategy was implemented to compute $f_{B_{s}}$.  It was also
performed for a double ratio to determine
$(f_{B_{s}}/f_{B})/(f_{K}/f_{\pi})$, hence $f_{B_{s}}/f_{B}$ using
information of $f_{K}/f_{\pi}$ from Ref.~\cite{Carrasco:2014poa}.  This double ratio leads to the
advantage that it contains small coefficients for chiral logarithms.
The $B$ meson decay constant is then computed with $f_{B} =
f_{B_{s}}/(f_{B_{s}}/f_{B})$.   The authors estimated various kinds of
systematic errors in their work.  These include discretisation errors,
the effects of perturbative matching between QCD and HQET, those of
chiral extrapolation, and errors associate with the value of $f_{K}/f_{\pi}$.
% end of the description of ETM 16B



% beginning of the description of HPQCD 17A
The authors of HPQCD 17A~\cite{Hughes:2017spc} re-analysed the data in
Ref.~\cite{Dowdall:2013tga} (HPQCD 13 in Tables~\ref{tab:FBssumm} and \ref{tab:FBratsumm}) employing a different method for computing $B$
decay constants with
NRQCD heavy quarks and HISQ light quarks on the lattice.  The NRQCD
action used in this work is improved to $O(\alpha_{s} v_{b}^{4})$, where
$v_{b}$ is the velocity of the $b$ quark.  In
Ref.~\cite{Dowdall:2013tga}, the determination of the decay
constants is carried out through studying matrix elements of axial currents.  On
the other hand, the same decay constants can be obtained by
investigating $(m_{b} + m_{l}) \la 0 | P | B \ra$, where $m_{b}$
($m_{l}$) is the $b$ (light) quark mass and $P$ stands for the
pseudoscalar current.   The matching of this pseudoscalar current
between QCD and NRQCD is performed at the precision of
${\mathcal{O}}(\alpha_{s})$,
${\mathcal{O}}(\alpha_{s}\Lambda_{{\mathrm{QCD}}}/m_{b})$ and
  ${\mathcal{O}}(\alpha_{s} a \Lambda_{{\mathrm{QCD}}})$, using lattice
    perturbation theory.   This requires the inclusion of three
    operators in the NRQCD-HISQ theory.    The gauge configurations
    used in this computation were part of those generated by the MILC collaboration,
    with details given in Ref.~\cite{Bazavov:2012xda}.   They are the
    ensembles obtained at three values of bare gauge coupling ($\beta
    = 6.3, 6.0$ and 5.8),
    corresponding to lattice spacings, determined using the $\Upsilon
    (2S-1A)$ splitting, between 0.09 and 0.15 fm.  Pion masses are
    between 128 and 315 MeV.  For each lattice spacing, the MILC
    collaboration performed simulations at several lattice volumes,
    such that $M_{\pi} L $ lies between 3.3 and 4.5 for all the data
    points.  This ensures that the finite-size effects are under
    control~\cite{Arndt:2004bg}. 
    On each ensemble, the bare NRQCD quark mass is tuned to
    the $b$-quark mass using the spin-average for the masses
    $\Upsilon$ and $\eta_{b}$.   In this work, a combined
    chiral-continuum extrapolation is performed, with the strategy of
    using Bayesian priors as explained in
    Ref.~\cite{Dowdall:2012ab}.   Systematic effects estimated
    in HPQCD 17A~\cite{Hughes:2017spc} include those from lattice-spacing dependence, the
    chiral fits, the $B{-}B^{\ast}{-}\pi$ axial coupling, the operator
    matching, and the relativistic corrections to the NRQCD
    formalism.  Although these errors are estimated in the same
    fashion as in Ref.~\cite{Dowdall:2013tga} (HPQCD
    13), most of them involve the handling of fits to the actual
    data.  This means that most of the systematics effects from
    HPQCD 13~\cite{Dowdall:2013tga} and HPQCD 17A~\cite{Hughes:2017spc} are not correlated, although the two
    calculations are performed on exactly the same gauge field
    ensembles.   The only exception is the error in the relativistic
    corrections.  For this, the authors simply take
    $(\Lambda_{{\mathrm{QCD}}}/m_{b})^{2} \approx 1\%$ as the
          estimation in both computations.  Therefore, we will correlate
          this part of the systematic effects in our
          average.\footnote{Following the guideline in
            Sec.~\ref{sec:error_analysis}, these systematic errors are
          assumed to be $100\%$ correlated.  It should be noted
          that this correlation cannot be taken at face value.}
% end  of the description of HPQCD 17A


% beginnig of the description of FNAL/MILC 17 
The third new calculation for the $B{-}$meson decay constants since
the last FLAG report was
performed by the FNAL/MILC collaboration (FNAL/MILC 17~\cite{Bazavov:2017lyh}  in Tables~\ref{tab:FBssumm}
and \ref{tab:FBratsumm}).   In this work, Ref.~\cite{Bazavov:2017lyh},
the simulations are performed for six lattice spacings, ranging from
0.03 to 0.15 fm.  For the two finest lattices ($a = 0.042$ and 0.03
fm), it is found that the topological charge is not well
equilibrated.  The effects of this non-equilibration are estimated
using results of chiral perturbation theory in
Ref.~\cite{Bernard:2017npd}.  Another feature of the simulations is
that both RHMC and RHMD algorithms are used.  The authors investigated
the effects of omitting the Metropolis test in the RHMD simulations by
examining changes of the plaquette values, and found that they do not
result in any variation with statistical significance.  The
light-quark masses used in this computation correspond to pion masses
between 130 to 314 MeV.   The values of the valence heavy-quark
mass $m_{h}$ are in the range of about 0.9 and 5 times the charm-quark mass.
Notice that on the two coarsest lattices, the authors only implement
calculations at $m_{h} \sim m_{c}$ in order to avoid uncontrolled
discretisation errors, while only on the two finest lattices, is $m_{h}$
chosen to be as high as $\sim 5 m_{c}$.  For setting the scale and
the quark masses, the approach described in Ref.~\cite{Bazavov:2014wgs} is employed,
with the special feature of using the decay constant of the ``fictitious'' meson that is
composed of degenerate quarks with mass $m_{p4s} = 0.4 m_{s}$.
The overall scale is determined by comparing $f_{\pi}$ to its PDG
average as reported in Ref.~\cite{Rosner:2015wva}.  In the analysis procedure
of extrapolating/interpolating to the physical quark masses and the continuum limit,
the key point is the use of heavy-meson rooted all-staggered chiral
perturbation theory (HMrA$\chi$PT)~\cite{Bernard:2013qwa}.  In order to account for lattice
artefacts and the effects of the heavy-quark mass in this chiral
expansion, appropriate polynomial terms in $a$ and $1/m_{h}$ are
included in the fit formulae.   The full NLO terms in the chiral
effective theory are incorporated in the analysis, while only the
analytic contributions from the NNLO are considered.  Furthermore,
data obtained at the coarsest lattice spacing, $a \approx 0.15$ fm,
are discarded for the central-value fits, and are 
subsequently used only for the estimation of systematic errors.  In this
analysis strategy, there are 60 free parameters to be determined by
about 500 data points.   Systematic errors estimated in FNAL/MILC
17  include excited-state contamination, choices of fit models with
different sizes of the priors, scale setting, quark-mass
tuning, finite-size correction, electromagnetic (EM) contribution,  and
topological non-equilibration.   The dominant effects are
from the first two in this list.  For the EM effects, the authors also
include an error associated with choosing a specific scheme to estimate
their contributions.  
% end of the description of FNAL/MILC 17 


In our current work, the averages for $f_{B_{s}}$, $f_{B^{0}}$
and $f_{B_{s}}/f_{B^{0}}$ with $N_{f}=2+1+1$ lattice simulations are updated,
because of the three published papers (FNAL/MILC
17 ~\cite{Bazavov:2017lyh}, HPQCD 17A~\cite{Hughes:2017spc} and ETM
  16B~\cite{Bussone:2016iua} in Tables~\ref{tab:FBssumm} and \ref{tab:FBratsumm}) that appeared after the release
of the last FLAG review~\cite{Aoki:2016frl}.   In the updated FLAG
averages, we
include results from these three new computations, as well as those in 
HPQCD 13~\cite{Dowdall:2013tga}.  Since the decay constants presented in HPQCD 13~\cite{Dowdall:2013tga}, HPQCD 17A~\cite{Hughes:2017spc} and FNAL/MILC
17  have been extracted with a significant overlap of gauge-field
configurations, we correlate statistical errors from these works.
Furthermore, as explained above, the systematic effects arising from
relativistic corrections in HPQCD 13~\cite{Dowdall:2013tga} and HPQCD 17A~\cite{Hughes:2017spc} are correlated.
Notice that the authors of FNAL/MILC 17~\cite{Bazavov:2017lyh}  computed
$f_{B_{s}}/f_{B^{+}}$ and $f_{B_{s}}/f_{B^{0}}$ without performing
an isospin average to obtain $f_{B_{s}}/f_{B}$.  This is the reason why
in Fig.~\ref{fig:fBratio} we show two results, FNAL/MILC 17 1
($f_{B_{s}}/f_{B^{0}}$) and FNAL/MILC 17 2 ($f_{B_{s}}/f_{B^{+}}$), from
this reference.   To determine the global average for
$f_{B_{s}}/f_{B}$, we first perform the average of
$f_{B_{s}}/f_{B^{+}}$ and $f_{B_{s}}/f_{B^{0}}$ in FNAL/MILC 17~\cite{Bazavov:2017lyh}  by
following the procedure in section~2, 
with all errors correlated.  This gives us the estimate of
$f_{B_{s}}/f_{B}$ from this work by the FNAL/MILC collaboration.


Following the above strategy, and the procedure explained in section~2, we compute the average of
$N_f = 2+1+1$ results for $f_{B_{(s)}}$ and $f_{B_{s}}/f_{B}$,
%
\begin{align}
&\label{eq:fB211}
\Nf=2+1+1:&\FLAGAVBEGIN f_{B} &= 190.0(1.3) \FLAGAVEND\;{\rm MeV}
&&\Refs~\mbox{\cite{Dowdall:2013tga,Bussone:2016iua,Hughes:2017spc,Bazavov:2017lyh}},\\
&\label{eq:fBs211}
\Nf=2+1+1: &\FLAGAVBEGIN f_{B_{s}} &= 230.3(1.3)  \FLAGAVEND\; {\rm MeV}
&&\Refs~\mbox{\cite{Dowdall:2013tga,Bussone:2016iua,Hughes:2017spc,Bazavov:2017lyh}}, \\
&\label{eq:fBratio211}
%\label{eq:fbav211}
\Nf=2+1+1: &\FLAGAVBEGIN f_{B_{s}}\over{f_B} &= 1.209(0.005)\FLAGAVEND
&&\Refs~\mbox{\cite{Dowdall:2013tga,Bussone:2016iua,Hughes:2017spc,Bazavov:2017lyh}}.
\end{align}

The PDG presented their averages for the
$N_{f}=2+1$ and $N_{f}=2+1+1$ lattice-QCD determinations of
$f_{B^{+}}$, $f_{B_{s}}$ and
$f_{B_{s}}/f_{B^{+}}$ in 2015~\cite{Rosner:2015wva}.  The $N_{f}=2+1$ lattice-computation results used in
Ref.~\cite{Rosner:2015wva} are identical to those included in our
current work.   Regarding our isospin-averaged $f_{B}$ as the
representative for $f_{B^{+}}$, then the current FLAG and
PDG estimations for these quantities are compatible, although the
errors of $N_{f}=2+1+1$ results in this report are significantly
smaller.   In the PDG
work, they ``corrected'' the isospin-averaged $f_{B}$, as reported by
various lattice collaborations, using the $N_{f}=2+1+1$ strong isospin-breaking effect
computed in HPQCD 13~\cite{Dowdall:2013tga} (see 
Table~\ref{tab:FBssumm} in this subsection).    However, since only
unitary points (with equal sea- and valence-quark masses) were considered in
HPQCD 13~\cite{Dowdall:2013tga}, this procedure only correctly accounts for the effect from the
valence-quark masses, while introducing a spurious sea-quark
contribution.   
%However, since the effects of the sea quarks emerge in all the
%calculations through the coupling with the gluons, the sea $u$
%and $d$ quark masses will always appear in the combination $(m^{({\rm sea})}_{u} +
%m^{({\rm sea})}_{d})$.  This means that the valence sector is normally
%the
%predominant source of strong isospin
%breaking~\cite{WalkerLoud:2009nf}
%\footnote{We thank Andre Walker-Loud for helpful discussion on this
%point.}.   
We notice that $f_{B^{+}}$ and $f_{B^{0}}$ are also
separately reported in FNAL/MILC 17~\cite{Bazavov:2017lyh} by
taking into account the strong-isospin effect, and it is found that these two
decay constants are well compatible.   Notice that the new FNAL/MILC
results were obtained by properly keeping the averaged light sea-quark
mass fixed when varying the quark masses in their analysis procedure.
Their finding indicates that the strong isospin breaking effects could
be smaller than what was suggested by previous computations.
%



# FLAG plot for BK
# Goes with first version of FLAG-3; 22/7/2015
# run with "python BK.py"
# HW: added ETMC 2+1+1 result
#
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm \hat{B}_K$"			# plot title
#plotnamestring	= "BK"			# filename for plot
plotnamestring	= "BK"			# filename for plot
plotxticks	= ([[0.7,0.75,0.8,0.85]])# locations for x-ticks
nminorticks	= 4			# number of intermediate minor ticks
					# 0 if none
yaxisstrings	= [				# x,y-location for y-strings
#		   [+0.44,10.0,"$N_f=2+1$"],
#		   [+0.44,2.5,"$N_f=2$"]
		   [+0.63,22.5,"$\\rm N_f=2+1+1$"],
		   [+0.63,11.5,"$\\rm N_f=2+1$"],
		   [+0.63,1.0,"$\\rm N_f=2$"]
		    ]
xlimits		= [0.65,0.90]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
#tpos = 1.1					# x-position for the data labels
tpos = 0.81					# x-position for the data labels
	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[0.717,0.024,0.024,0.000,0.000,  "ETM 15"	,['s','g','g',0,tpos]]
]
dat2p1=[
#[0.83,	0.18,0.18,0.00,0.00,  "HPQCD/UKQCD 06" 		,['s','r','r',0,tpos]],
#[0.720,	0.039,0.039,0.013,0.013,  "RBC/UKQCD 07A, 08"	,['s','w','r',0,tpos]],
[0.724,	0.030,0.030,0.008,0.008,  "Aubin 09"  	        ,['s','l','g',0,tpos]],
[0.724,	0.045,0.045,0.012,0.012,  "SWME 10"    	        ,['s','l','g',0,tpos]],
[0.749,	0.027,0.027,0.007,0.007,  "RBC/UKQCD 10B" 	,['s','l','g',0,tpos]],
[0.7727,0.0117,0.0117,0.0081,0.0081,  "BMW 11" 		,['s','g','g',0,tpos]],
[0.727,	0.038,0.038,0.004,0.004,  "SWME 11A"	,['s','l','g',0,tpos]],
[0.7628,0.0208,0.0208,0.0038,0.0038,  "Laiho 11"	,['s','g','g',0,tpos]],
[0.758,	0.022,0.022,0.011,0.011,  "RBC/UKQCD 12A"	,['s','l','g',0,tpos]],
[0.738,	0.034,0.034,0.005,0.005,  "SWME 13"	,['s','l','g',0,tpos]],
[0.735,	0.033,0.033,0.010,0.010,  "SWME 13A"	,['s','l','g',0,tpos]],
[0.7379,0.0368,0.0368,0.0047,0.0047,  "SWME 14"	,['s','l','g',0,tpos]],
[0.7499,0.0152,0.0152,0.0024,0.0024,  "RBC/UKQCD 14B"	,['s','g','g',0,tpos]],
[0.7345,0.0368,0.0366,0.0049,0.0049,  "SWME 15A"	,['s','g','g',0,tpos]],
[0.744,0.022,0.022,0.013,0.013,  "RBC/UKQCD 16"	,['s','l','g',0,tpos]]
]
dat2=[                          
#[0.68,  0.18,0.18,0.0,0.0,    "UKQCD 04"      	        ,['s','w','r',0,tpos]],
#[0.678,	0.025,0.025,0.0,0.0,    "RBC 04" 		,['s','w','r',0,tpos]],
#[0.758,	0.071,0.071,0.006,0.006,  "JLQCD 08" 		,['s','w','r',0,tpos]], 
[0.729,	0.030,0.030,0.025,0.025, "ETM 10A" 		,['s','l','g',0,tpos]],
[0.727,	0.025,0.025,0.022,0.022, "ETM 12D" 		,['s','g','g',0,tpos]] 
]

# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[0.727,0.025,0.025,0.00,0.000,"FLAG average for $\\rm N_f=2$",['s','k','k',0,tpos],0],
[0.7627,0.0097,0.0097,0.00,0.000,"FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[0.717,0.024,0.024,0.00,0.000,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('./FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker



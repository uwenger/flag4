version=`svn status -u |awk '/Status/ {print $4}'`
cd ../
tar -cvzf FLAG2_Review_v$version.tar.gz FLAG2_Review/FLAG.bib FLAG2_Review/*.tex \
FLAG2_Review/Glossary/*.tex \
FLAG2_Review/HQ/*.tex \
FLAG2_Review/HQ/AppendixTables/*.tex \
FLAG2_Review/HQ/HQSubsections/*.tex \
FLAG2_Review/HQ/Plots/*.eps \
FLAG2_Review/HQ/Plots/*.pdf \
FLAG2_Review/HQ/Plots/Python/*.py \
FLAG2_Review/HQ/macros_static.sty \
FLAG2_Review/LEC/*.tex \
FLAG2_Review/LEC/Figures/*.eps \
FLAG2_Review/LEC/Figures/*.pdf \
FLAG2_Review/LEC/Figures/*.py \
FLAG2_Review/Vudus/*.tex \
FLAG2_Review/Vudus/Figures/*.eps \
FLAG2_Review/Vudus/Figures/*.pdf \
FLAG2_Review/Vudus/Figures/*.py \
FLAG2_Review/qmass/*.tex \
FLAG2_Review/qmass/Figures/*.eps \
FLAG2_Review/qmass/Figures/*.pdf \
FLAG2_Review/qmass/Figures/*.py \
FLAG2_Review/BK/*.tex \
FLAG2_Review/BK/Figures/*.eps \
FLAG2_Review/BK/Figures/*.pdf \
FLAG2_Review/BK/Figures/*.py \
FLAG2_Review/Alpha_s/*.tex \
FLAG2_Review/Alpha_s/Figures/*.py \
FLAG2_Review/Alpha_s/Figures/*.pdf \
FLAG2_Review/Alpha_s/Figures/*.eps \
FLAG2_Review/Makefile \
FLAG2_Review/defs.tex \
FLAG2_Review/hypernat.sty \
#FLAG2_Review/macros_static.sty \
FLAG2_Review/JHEP_FLAG.bst
#gzip FLAG2_Review_v$version.tar
cd FLAG2_Review

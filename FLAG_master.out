\BOOKMARK [1][-]{section.1}{Introduction}{}% 1
\BOOKMARK [2][-]{subsection.1.1}{FLAG composition, guidelines and rules}{section.1}% 2
\BOOKMARK [2][-]{subsection.1.2}{Citation policy}{section.1}% 3
\BOOKMARK [2][-]{subsection.1.3}{General issues}{section.1}% 4
\BOOKMARK [1][-]{section.2}{Quality criteria, averaging and error estimation}{}% 5
\BOOKMARK [2][-]{subsection.2.1}{Systematic errors and colour code}{section.2}% 6
\BOOKMARK [3][-]{subsubsection.2.1.1}{Systematic effects and rating criteria}{subsection.2.1}% 7
\BOOKMARK [3][-]{subsubsection.2.1.2}{Heavy-quark actions}{subsection.2.1}% 8
\BOOKMARK [3][-]{subsubsection.2.1.3}{Conventions for the figures}{subsection.2.1}% 9
\BOOKMARK [2][-]{subsection.2.2}{Averages and estimates}{section.2}% 10
\BOOKMARK [2][-]{subsection.2.3}{Averaging procedure and error analysis}{section.2}% 11
\BOOKMARK [3][-]{subsubsection.2.3.1}{Averaging \204 generic case}{subsection.2.3}% 12
\BOOKMARK [3][-]{subsubsection.2.3.2}{Nested averaging}{subsection.2.3}% 13
\BOOKMARK [1][-]{section.3}{Quark masses}{}% 14
\BOOKMARK [2][-]{subsection.3.1}{Masses of the light quarks}{section.3}% 15
\BOOKMARK [3][-]{subsubsection.3.1.1}{The physical point and isospin symmetry}{subsection.3.1}% 16
\BOOKMARK [3][-]{subsubsection.3.1.2}{Ambiguities in the separation of strong and electromagnetic contributions}{subsection.3.1}% 17
\BOOKMARK [3][-]{subsubsection.3.1.3}{Inclusion of electromagnetic effects in lattice QCD simulations}{subsection.3.1}% 18
\BOOKMARK [3][-]{subsubsection.3.1.4}{Lattice determination of ms and mud}{subsection.3.1}% 19
\BOOKMARK [3][-]{subsubsection.3.1.5}{Lattice determinations of ms/mud}{subsection.3.1}% 20
\BOOKMARK [3][-]{subsubsection.3.1.6}{Lattice determination of mu and md}{subsection.3.1}% 21
\BOOKMARK [3][-]{subsubsection.3.1.7}{Estimates for R and Q}{subsection.3.1}% 22
\BOOKMARK [2][-]{subsection.3.2}{Charm quark mass}{section.3}% 23
\BOOKMARK [3][-]{subsubsection.3.2.1}{Nf = 2+1 results}{subsection.3.2}% 24
\BOOKMARK [3][-]{subsubsection.3.2.2}{Nf = 2+1+1 results}{subsection.3.2}% 25
\BOOKMARK [3][-]{subsubsection.3.2.3}{Lattice determinations of the ratio mc/ms}{subsection.3.2}% 26
\BOOKMARK [2][-]{subsection.3.3}{Bottom-quark mass}{section.3}% 27
\BOOKMARK [3][-]{subsubsection.3.3.1}{Nf=2+1}{subsection.3.3}% 28
\BOOKMARK [3][-]{subsubsection.3.3.2}{Nf=2+1+1}{subsection.3.3}% 29
\BOOKMARK [1][-]{section.4}{Leptonic and semileptonic kaon and pion decay and |Vud| and |Vus|}{}% 30
\BOOKMARK [2][-]{subsection.4.1}{Experimental information concerning |Vud|, |Vus|, f+\(0\) and \040fK/f}{section.4}% 31
\BOOKMARK [2][-]{subsection.4.2}{Lattice results for f+\(0\) and fK/f}{section.4}% 32
\BOOKMARK [2][-]{subsection.4.3}{Direct determination of f+\(0\) and fK/f}{section.4}% 33
\BOOKMARK [2][-]{subsection.4.4}{Tests of the Standard Model}{section.4}% 34
\BOOKMARK [2][-]{subsection.4.5}{Analysis within the Standard Model}{section.4}% 35
\BOOKMARK [2][-]{subsection.4.6}{Direct determination of fK and f}{section.4}% 36
\BOOKMARK [1][-]{section.5}{Low-energy constants}{}% 37
\BOOKMARK [2][-]{subsection.5.1}{Chiral perturbation theory }{section.5}% 38
\BOOKMARK [3][-]{subsubsection.5.1.1}{Quark-mass dependence of pseudoscalar masses and decay constants}{subsection.5.1}% 39
\BOOKMARK [3][-]{subsubsection.5.1.2}{Pion form factors and charge radii}{subsection.5.1}% 40
\BOOKMARK [3][-]{subsubsection.5.1.3}{Goldstone boson scattering in a finite volume}{subsection.5.1}% 41
\BOOKMARK [3][-]{subsubsection.5.1.4}{Partially quenched and mixed action formulations}{subsection.5.1}% 42
\BOOKMARK [3][-]{subsubsection.5.1.5}{Correlation functions in the -regime}{subsection.5.1}% 43
\BOOKMARK [3][-]{subsubsection.5.1.6}{Energy levels of the QCD Hamiltonian in a box and -regime}{subsection.5.1}% 44
\BOOKMARK [3][-]{subsubsection.5.1.7}{Other methods for the extraction of the low-energy constants}{subsection.5.1}% 45
\BOOKMARK [2][-]{subsection.5.2}{Extraction of SU\(2\) low-energy constants }{section.5}% 46
\BOOKMARK [3][-]{subsubsection.5.2.1}{Results for the LO SU\(2\) LECs }{subsection.5.2}% 47
\BOOKMARK [3][-]{subsubsection.5.2.2}{Results for the NLO SU\(2\) LECs }{subsection.5.2}% 48
\BOOKMARK [3][-]{subsubsection.5.2.3}{Epilogue}{subsection.5.2}% 49
\BOOKMARK [2][-]{subsection.5.3}{Extraction of SU\(3\) low-energy constants }{section.5}% 50
\BOOKMARK [3][-]{subsubsection.5.3.1}{Results for the LO and NLO SU\(3\) LECs}{subsection.5.3}% 51
\BOOKMARK [3][-]{subsubsection.5.3.2}{Epilogue}{subsection.5.3}% 52
\BOOKMARK [3][-]{subsubsection.5.3.3}{Outlook}{subsection.5.3}% 53
\BOOKMARK [1][-]{section.6}{Kaon mixing}{}% 54
\BOOKMARK [2][-]{subsection.6.1}{Indirect CP violation and K in the SM }{section.6}% 55
\BOOKMARK [2][-]{subsection.6.2}{Lattice computation of BK}{section.6}% 56
\BOOKMARK [2][-]{subsection.6.3}{Kaon BSM B parameters}{section.6}% 57
\BOOKMARK [1][-]{section.7}{D-meson decay constants and form factors}{}% 58
\BOOKMARK [2][-]{subsection.7.1}{Leptonic decay constants fD and fDs}{section.7}% 59
\BOOKMARK [2][-]{subsection.7.2}{Form factors for D and DK \040semileptonic decays}{section.7}% 60
\BOOKMARK [2][-]{subsection.7.3}{Form factors for c semileptonic decays}{section.7}% 61
\BOOKMARK [2][-]{subsection.7.4}{Determinations of |Vcd| and |Vcs| and test of second-row CKM unitarity}{section.7}% 62
\BOOKMARK [1][-]{section.8}{B-meson decay constants, mixing parameters and form factors}{}% 63
\BOOKMARK [2][-]{subsection.8.1}{Leptonic decay constants fB and fBs}{section.8}% 64
\BOOKMARK [2][-]{subsection.8.2}{Neutral B-meson mixing matrix elements}{section.8}% 65
\BOOKMARK [3][-]{subsubsection.8.2.1}{Error treatment for B meson bag parameters}{subsection.8.2}% 66
\BOOKMARK [2][-]{subsection.8.3}{Semileptonic form factors for B decays to light flavours}{section.8}% 67
\BOOKMARK [3][-]{subsubsection.8.3.1}{Form factors for B}{subsection.8.3}% 68
\BOOKMARK [3][-]{subsubsection.8.3.2}{Form factors for BsK}{subsection.8.3}% 69
\BOOKMARK [3][-]{subsubsection.8.3.3}{Form factors for rare and radiative B-semileptonic decays to light flavours}{subsection.8.3}% 70
\BOOKMARK [2][-]{subsection.8.4}{Semileptonic form factors for B\(s\) D\(s\) \040and B D* }{section.8}% 71
\BOOKMARK [3][-]{subsubsection.8.4.1}{ B\(s\) D\(s\) decays}{subsection.8.4}% 72
\BOOKMARK [3][-]{subsubsection.8.4.2}{Ratios of BD form factors}{subsection.8.4}% 73
\BOOKMARK [3][-]{subsubsection.8.4.3}{B D* decays}{subsection.8.4}% 74
\BOOKMARK [2][-]{subsection.8.5}{Semileptonic form factors for Bcc and BcJ/}{section.8}% 75
\BOOKMARK [2][-]{subsection.8.6}{Semileptonic form factors for bp and bc}{section.8}% 76
\BOOKMARK [2][-]{subsection.8.7}{Determination of |Vub|}{section.8}% 77
\BOOKMARK [2][-]{subsection.8.8}{Determination of |Vcb|}{section.8}% 78
\BOOKMARK [1][-]{section.9}{The strong coupling s}{}% 79
\BOOKMARK [2][-]{subsection.9.1}{Introduction}{section.9}% 80
\BOOKMARK [3][-]{subsubsection.9.1.1}{Scheme and scale dependence of s and QCD}{subsection.9.1}% 81
\BOOKMARK [3][-]{subsubsection.9.1.2}{Overview of the review of s}{subsection.9.1}% 82
\BOOKMARK [3][-]{subsubsection.9.1.3}{Additions to the FLAG 13 report}{subsection.9.1}% 83
\BOOKMARK [3][-]{subsubsection.9.1.4}{Additions to the FLAG 16 report}{subsection.9.1}% 84
\BOOKMARK [2][-]{subsection.9.2}{General issues}{section.9}% 85
\BOOKMARK [3][-]{subsubsection.9.2.1}{Discussion of criteria for computations entering the averages}{subsection.9.2}% 86
\BOOKMARK [3][-]{subsubsection.9.2.2}{Physical scale}{subsection.9.2}% 87
\BOOKMARK [3][-]{subsubsection.9.2.3}{Studies of truncation errors of perturbation theory}{subsection.9.2}% 88
\BOOKMARK [2][-]{subsection.9.3}{s from Step Scaling Methods}{section.9}% 89
\BOOKMARK [3][-]{subsubsection.9.3.1}{General considerations}{subsection.9.3}% 90
\BOOKMARK [3][-]{subsubsection.9.3.2}{Discussion of computations}{subsection.9.3}% 91
\BOOKMARK [2][-]{subsection.9.4}{s from the potential at short distances}{section.9}% 92
\BOOKMARK [3][-]{subsubsection.9.4.1}{General considerations}{subsection.9.4}% 93
\BOOKMARK [3][-]{subsubsection.9.4.2}{Discussion of computations}{subsection.9.4}% 94
\BOOKMARK [2][-]{subsection.9.5}{s from the vacuum polarization at short distances}{section.9}% 95
\BOOKMARK [3][-]{subsubsection.9.5.1}{General considerations}{subsection.9.5}% 96
\BOOKMARK [3][-]{subsubsection.9.5.2}{Discussion of computations}{subsection.9.5}% 97
\BOOKMARK [2][-]{subsection.9.6}{s from observables at the lattice spacing scale}{section.9}% 98
\BOOKMARK [3][-]{subsubsection.9.6.1}{General considerations}{subsection.9.6}% 99
\BOOKMARK [3][-]{subsubsection.9.6.2}{Continuum limit}{subsection.9.6}% 100
\BOOKMARK [3][-]{subsubsection.9.6.3}{Discussion of computations}{subsection.9.6}% 101
\BOOKMARK [2][-]{subsection.9.7}{s from current two-point functions}{section.9}% 102
\BOOKMARK [3][-]{subsubsection.9.7.1}{General considerations}{subsection.9.7}% 103
\BOOKMARK [3][-]{subsubsection.9.7.2}{Discussion of computations}{subsection.9.7}% 104
\BOOKMARK [2][-]{subsection.9.8}{s from QCD vertices}{section.9}% 105
\BOOKMARK [3][-]{subsubsection.9.8.1}{General considerations}{subsection.9.8}% 106
\BOOKMARK [3][-]{subsubsection.9.8.2}{Discussion of computations}{subsection.9.8}% 107
\BOOKMARK [2][-]{subsection.9.9}{s from the eigenvalue spectrum of the Dirac operator}{section.9}% 108
\BOOKMARK [3][-]{subsubsection.9.9.1}{General considerations}{subsection.9.9}% 109
\BOOKMARK [3][-]{subsubsection.9.9.2}{Discussion of computations}{subsection.9.9}% 110
\BOOKMARK [2][-]{subsection.9.10}{Summary}{section.9}% 111
\BOOKMARK [3][-]{subsubsection.9.10.1}{The present situation}{subsection.9.10}% 112
\BOOKMARK [3][-]{subsubsection.9.10.2}{Our range for MS\(5\)}{subsection.9.10}% 113
\BOOKMARK [3][-]{subsubsection.9.10.3}{Ranges for [r0 ]\(N f\) and MS}{subsection.9.10}% 114
\BOOKMARK [3][-]{subsubsection.9.10.4}{Conclusions}{subsection.9.10}% 115
\BOOKMARK [1][-]{section.10}{Nucleon matrix elements}{}% 116
\BOOKMARK [2][-]{subsection.10.1}{Isovector and flavor diagonal charges of the nucleon}{section.10}% 117
\BOOKMARK [3][-]{subsubsection.10.1.1}{Technical aspects of the calculations of NME}{subsection.10.1}% 118
\BOOKMARK [3][-]{subsubsection.10.1.2}{Controlling excited-state contamination}{subsection.10.1}% 119
\BOOKMARK [3][-]{subsubsection.10.1.3}{Renormalization and Symanzik improvement of local currents}{subsection.10.1}% 120
\BOOKMARK [3][-]{subsubsection.10.1.4}{Extrapolations in a, M and ML}{subsection.10.1}% 121
\BOOKMARK [2][-]{subsection.10.2}{Quality criteria for nucleon matrix elements and averaging procedure }{section.10}% 122
\BOOKMARK [2][-]{subsection.10.3}{Isovector charges}{section.10}% 123
\BOOKMARK [3][-]{subsubsection.10.3.1}{Results for gAu-d}{subsection.10.3}% 124
\BOOKMARK [3][-]{subsubsection.10.3.2}{Results for gSu-d}{subsection.10.3}% 125
\BOOKMARK [3][-]{subsubsection.10.3.3}{Results for gTu-d}{subsection.10.3}% 126
\BOOKMARK [2][-]{subsection.10.4}{Flavor Diagonal Charges}{section.10}% 127
\BOOKMARK [3][-]{subsubsection.10.4.1}{Results for gAu,d,s}{subsection.10.4}% 128
\BOOKMARK [3][-]{subsubsection.10.4.2}{Results for gSu,d,s from direct calculations of the matrix elements}{subsection.10.4}% 129
\BOOKMARK [3][-]{subsubsection.10.4.3}{Results for gSu,d,s using the Feynman-Hellmann theorem}{subsection.10.4}% 130
\BOOKMARK [3][-]{subsubsection.10.4.4}{Summary of Results for gSu,d}{subsection.10.4}% 131
\BOOKMARK [3][-]{subsubsection.10.4.5}{Results for gTu,d,s}{subsection.10.4}% 132
\BOOKMARK [1][-]{section*.2}{Acknowledgments}{}% 133
\BOOKMARK [1][-]{appendix.A}{Glossary}{}% 134
\BOOKMARK [2][-]{subsection.A.1}{Lattice actions}{appendix.A}% 135
\BOOKMARK [3][-]{subsubsection.A.1.1}{Gauge actions }{subsection.A.1}% 136
\BOOKMARK [3][-]{subsubsection.A.1.2}{Light-quark actions }{subsection.A.1}% 137
\BOOKMARK [3][-]{subsubsection.A.1.3}{Heavy-quark actions}{subsection.A.1}% 138
\BOOKMARK [2][-]{subsection.A.2}{Setting the scale }{appendix.A}% 139
\BOOKMARK [2][-]{subsection.A.3}{Matching and running }{appendix.A}% 140
\BOOKMARK [2][-]{subsection.A.4}{Chiral extrapolation}{appendix.A}% 141
\BOOKMARK [2][-]{subsection.A.5}{Parameterizations of semileptonic form factors}{appendix.A}% 142
\BOOKMARK [2][-]{subsection.A.6}{Summary of simulated lattice actions}{appendix.A}% 143
\BOOKMARK [1][-]{appendix.B}{Notes}{}% 144
\BOOKMARK [2][-]{subsection.B.1}{Notes to section 3 on quark masses}{appendix.B}% 145
\BOOKMARK [2][-]{subsection.B.2}{Notes to section 4 on |Vud| and |Vus|}{appendix.B}% 146
\BOOKMARK [2][-]{subsection.B.3}{Notes to section 5 on Low-Energy Constants}{appendix.B}% 147
\BOOKMARK [2][-]{subsection.B.4}{Notes to section 6 on Kaon mixing}{appendix.B}% 148
\BOOKMARK [3][-]{subsubsection.B.4.1}{Kaon B-parameter BK}{subsection.B.4}% 149
\BOOKMARK [3][-]{subsubsection.B.4.2}{Kaon BSM B-parameters}{subsection.B.4}% 150
\BOOKMARK [2][-]{subsection.B.5}{Notes to section 7 on D-meson decay constants and form factors}{appendix.B}% 151
\BOOKMARK [3][-]{subsubsection.B.5.1}{Form factors for semileptonic decays of charmed hadrons}{subsection.B.5}% 152
\BOOKMARK [2][-]{subsection.B.6}{Notes to section 8 on B-meson decay constants, mixing parameters and form factors}{appendix.B}% 153
\BOOKMARK [3][-]{subsubsection.B.6.1}{B\(s\)-meson decay constants}{subsection.B.6}% 154
\BOOKMARK [3][-]{subsubsection.B.6.2}{B\(s\)-meson mixing matrix elements}{subsection.B.6}% 155
\BOOKMARK [3][-]{subsubsection.B.6.3}{Form factors entering determinations of |Vub| \(B l, Bs K l, b pl\)}{subsection.B.6}% 156
\BOOKMARK [3][-]{subsubsection.B.6.4}{Form factors for rare decays of beauty hadrons}{subsection.B.6}% 157
\BOOKMARK [3][-]{subsubsection.B.6.5}{Form factors entering determinations of |Vcb| \(B\(s\) D\(s\)\(*\) l, b c l\) and R\(D\(s\)\)}{subsection.B.6}% 158
\BOOKMARK [2][-]{subsection.B.7}{Notes to section 9 on the strong coupling s}{appendix.B}% 159
\BOOKMARK [3][-]{subsubsection.B.7.1}{Renormalization scale and perturbative behaviour}{subsection.B.7}% 160
\BOOKMARK [3][-]{subsubsection.B.7.2}{Continuum limit}{subsection.B.7}% 161
\BOOKMARK [2][-]{subsection.B.8}{Notes to section 10 on nucleon matrix elements}{appendix.B}% 162

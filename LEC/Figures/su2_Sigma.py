# FLAG plot for Sigma
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\Sigma^{1/3}$"			# plot title
plotnamestring	= "su2_Sigma"			# filename for plot
plotxticks	= ([[200, 250,  300,  350]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [+190,37.5,"$\\rm N_f=2+1+1$"],
		   [+190,24.0,"$\\rm N_f=2+1$"],
		   [+190,07.0,"$\\rm N_f=2$"],
		    ]
LABEL=1
xaxisstrings	= [				# x,y-location for x-strings
		   [+405.,-3.0,"MeV"] 	 		    
                   ]		   

xlimits		= [200,420]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 350					# x-position for the data labels

nan=np.nan

# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

dat2p1p1=[
 [280  ,8   ,8   ,17.0  ,17.0  , "ETM 13"                , ['*','g','g',0,tpos]],
 [318  ,21  ,21  ,29.7  ,29.7  , "ETM 17E"               , ['*','g','g',0,tpos]]
]

dat2p1=[
 [255  ,8   ,8   ,17.2  ,17.2  , "RBC/UKQCD 08"          , ['s','w','r',0,tpos]],
#[309  ,7   ,7   ,np.nan,np.nan, "PACS-CS 08, SU(2)-fit" , ['s','w','r',0,tpos]], # no systematic error
#[312  ,10  ,10  ,np.nan,np.nan, "PACS-CS 08, SU(3)-fit" , ['s','w','r',0,tpos]], # no systematic error
 [253  ,4   ,4   ,7.2   ,7.2   , "JLQCD/TWQCD 08B"       , ['^','w','r',0,tpos]],
 [259  ,6   ,6   ,10.8  ,10.8  , "TWQCD 08"              , ['^','w','r',0,tpos]],
 [278  ,1   ,1   ,5.9   ,5.5   , "MILC 09"               , ['s','l','g',0,tpos]],
 [280  ,2   ,2   ,9     ,6     , "MILC 09A, $SU(2)$-fit" , ['s','l','g',0,tpos]],
 [279  ,1   ,1   ,4.6   ,4.6   , "MILC 09A, $SU(3)$-fit" , ['s','l','g',0,tpos]],
 [242  ,4   ,4   ,18.44 ,19.42 , "JLQCD 09"              , ['s','w','r',0,tpos]],
 [256  ,5   ,5   ,5.7   ,5.7   , "RBC/UKQCD 10A"         , ['s','w','r',0,tpos]],
 [234  ,4   ,4   ,17.5  ,17.5  , "JLQCD/TWQCD 10A"       , ['s','w','r',0,tpos]],
 [281.5,3.4 ,3.4 ,7.9   ,5.6   , "MILC 10A"              , ['s','g','g',0,tpos]],
 [272.3,1.2 ,1.2 ,1.8   ,1.8   , "Borsanyi 12"           , ['s','g','g',0,tpos]],
 [271.0,4.0 ,4.0 ,4.1   ,4.1   , "BMW 13"                , ['s','g','g',0,tpos]],
 [275.9,1.9 ,1.9 ,2.1   ,2.1   , "RBC/UKQCD 14B"         , ['s','l','g',0,tpos]],
 [274.2,2.8 ,2.8 ,4.88  ,4.88  , "RBC/UKQCD 15E"         , ['s','g','g',0,tpos]],
 [270.0,1.3 ,1.3 ,4.97  ,4.97  , "JLQCD 16B"             , ['*','g','g',0,tpos]],
 [274.0,13.0,13.0,31.8  ,31.8  , "JLQCD 17A"             , ['^','g','g',0,tpos]]
]

dat2=[
#[239.8,4   ,4   ,np.nan,np.nan, "JLQCD/TWQCD 07"        , ['<','w','r',0,tpos]], # no systematic error
 [252  ,5   ,5   ,11.2  ,11.2  , "JLQCD/TWQCD 07A"       , ['^','w','r',0,tpos]],
 [235.7,5   ,5   ,5.4   ,13.8  , "JLQCD/TWQCD 08A"       , ['s','w','r',0,tpos]],
 [276  ,3   ,3   ,7.1   ,7.1   , "CERN 08"               , ['s','w','r',0,tpos]],
 [264  ,3   ,3   ,5.8   ,5.8   , "ETM 08"                , ['D','l','g',0,tpos]],
#[248  ,6   ,6   ,np.nan,np.nan, "Hasenfratz 08"         , ['<','w','r',0,tpos]], # no systematic error
#[245  ,5   ,5   ,np.nan,np.nan, "ETM 09B"               , ['<','w','r',0,tpos]], # no systematic error
 [270  ,5   ,5   ,6.4   ,5.8   , "ETM 09C"               , ['s','g','g',0,tpos]],
 [262  ,34  ,33  ,34    ,33    , "Bernardoni 10"         , ['^','w','r',0,tpos]],
 [242  ,5   ,5   ,20.6  ,20.6  , "JLQCD/TWQCD 10A"       , ['s','w','r',0,tpos]],
 [259  ,6   ,6   ,9     ,9     , "TWQCD 11A"             , ['^','w','r',0,tpos]],
 [230  ,4   ,4   ,7.2   ,7.2   , "TWQCD 11"              , ['s','w','r',0,tpos]], # was 235,8,8,9,9 by mistake in FLAG2
#[306  ,11  ,11  ,np.nan,np.nan, "Bernardoni 11"         , ['s','w','r',0,tpos]], # no systematic error
 [299  ,26  ,26  ,39    ,39    , "ETM 12"                , ['s','l','g',0,tpos]],
 [283  ,7   ,7   ,18    ,18    , "ETM 13"                , ['*','g','g',0,tpos]],
 [261  ,13  ,13  ,13.04 ,13.04 , "Brandt 13"             , ['D','g','g',0,tpos]],
 [263  ,3   ,3   ,5     ,5     , "Engel 14"              , ['s','g','g',0,tpos]]  # SD-GOR-hybrid, but latter dominates
]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN" with respect to the above
	      # there is another column at the end specifying the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [ 265.7, 9.6, 9.6, 3.8, 3.8, "FLAG estimate for $\\rm N_f=2$",     ['s','k','k',0,tpos],0],
 [ 272.5, 4.9, 4.9, 1.5, 1.5, "FLAG estimate for $\\rm N_f=2+1$",   ['s','k','k',0,tpos],0],
 [ 285.5,23.5,23.5,16.5,16.5, "FLAG estimate for $\\rm N_f=2+1+1$", ['s','k','k',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker


# FLAG plot for l3bar
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\bar{\\ell}_3$"			# plot title
plotnamestring	= "su2_l3bar"			# filename for plot
plotxticks	= ([[1, 2, 3, 4, 5]])# locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [-0.4,26.6,"$\\rm N_f=2+1+1$"],
		   [-0.4,17.0,"$\\rm N_f=2+1$"],
		   [-0.4,06.0,"$\\rm N_f=2$"]
		    ]
xlimits		= [0,9]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 6					# x-position for the data labels

# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
 [3.7 ,0.07,0.07,0.27,0.27,       "ETM 10",               ['s','w','r',0,tpos]],
 [3.53,0.05,0.05,0.26,0.26,       "ETM 11",               ['s','g','g',0,tpos]]  # update to and systematic error taken from ETM 10
]

dat2p1=[
 [3.13,0.33,0.33,0.41,0.41,       "RBC/UKQCD 08",         ['s','w','r',0,tpos]],
#[3.47,0.11,0.11,0.11,0.11,       "PACS-CS 08",           ['s','w','r',0,tpos]],
 [3.0 ,0.6 ,0.6 ,0.84,1.08,       "MILC 09A, $SU(2)$-fit",['s','l','g',0,tpos]],
 [3.32,0.64,0.64,0.78,0.78,       "MILC 09A, $SU(3)$-fit",['s','l','g',0,tpos]],
#[2.57,0.18,0.18,0.32,0.32,       "RBC/UKQCD 10A",        ['s','g','g',0,tpos]], # where does this syst error come from ?
 [2.85,0.81,0.81,1.23,0.89,       "MILC 10A",             ['s','l','g',0,tpos]],
 [3.18,0.50,0.50,1.02,1.02,       "MILC 10",              ['s','g','g',0,tpos]],
 [4.04,0.40,0.40,0.68,0.83,       "NPLQCD 11",            ['s','g','g',0,tpos]],
 [3.16,0.10,0.10,0.31,0.31,       "Borsanyi 12",          ['s','g','g',0,tpos]],
 [2.91,0.23,0.23,0.24,0.24,       "RBC/UKQCD 12",         ['s','l','g',0,tpos]],
 [2.50,0.50,0.50,0.64,0.64,       "BMW 13",               ['s','g','g',0,tpos]],
 [2.73,0.13,0.13,0.13,0.13,       "RBC/UKQCD 14B",        ['s','l','g',0,tpos]],
 [2.81,0.19,0.19,0.49,0.49,       "RBC/UKQCD 15E",        ['s','g','g',0,tpos]]
]

dat2=[
 [3.0  ,0.5  ,0.5  ,0.51 ,0.51 ,  "CERN-TOV 06",          ['s','w','r',0,tpos]],
 [3.38 ,0.4  ,0.4  ,0.47 ,0.56 ,  "JLQCD/TWQCD 08A",      ['s','w','r',0,tpos]],
 [3.2  ,0.8  ,0.8  ,0.83 ,0.83 ,  "ETM 08",               ['D','g','g',0,tpos]],
 [3.5  ,0.09 ,0.09 ,0.31 ,0.13 ,  "ETM 09C",              ['s','g','g',0,tpos]],
 [4.149,0.035,0.035,0.038,0.038,  "TWQCD 11",             ['s','w','r',0,tpos]],
 [4.46 ,0.30 ,0.30 ,0.33 ,0.33 ,  "Bernardoni 11",        ['s','w','r',0,tpos]],
 [3.0  ,0.7  ,0.7  ,0.86 ,0.86 ,  "Brandt 13",            ['D','g','g',0,tpos]]
]

datphen=[
[2.9,2.4,2.4,2.4,2.4,"Gasser 84",['o','b','b',0,tpos]]
]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) # is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN" with respect to the above
	      # there is another column at the end specifying the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [np.nan,0.00,0.00,0.00,0.00, "placeholder pheno",                 ['s','k','k',0,tpos],0],
 [  3.41,0.82,0.82,0.25,0.25, "FLAG estimate for $\\rm N_f=2$",    ['s','k','k',0,tpos],0],
 [  3.07,0.64,0.64,0.23,0.23, "FLAG estimate for $\\rm N_f=2+1$",  ['s','k','k',0,tpos],0],
 [  3.53,0.26,0.26,0.26,0.26, "FLAG average for $\\rm N_f=2+1+1$", ['s','k','k',0,tpos],0]
]

### ETM 09C: [3.5,0.31,0.13] --> [3.5-0.5*0.09,0.31-0.5*0.09,0.13+1.5*0.09]=[3.455,0.265,0.265]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datphen,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker


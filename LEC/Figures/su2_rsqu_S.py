# FLAG plot for r^2_S
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\rm<\,r^2>_S^{\pi}$"         # plot title
plotnamestring	= "su2_rsqu_S"			# filename for plot
plotxticks	= ([[0.5,0.6,0.7]])      # locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [0.4, 5.4,"$\\rm N_f=2+1+1$"],
#		   [0.4, 4.0,"$\\rm N_f=2+1$"],
		   [0.4, 2.1,"$\\rm N_f=2$"]
		    ]
xlimits		= [0.4,0.85]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 0.75					# x-position for the data labels
SHIFT=-.1
# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak

dat2p1p1=[
 [0.481, 0.037,0.037, 0.0622,0.0622, "HPQCD 15B"           , ['s','w','g',0,tpos]]
]

#dat2p1=[
# [0.395, 0.026,0.026, 0.0412,0.0412, "JLQCD 15A"           , ['s','w','r',0,tpos]]
#]

dat2=[
 [0.617, 0.079,0.079, 0.1029,0.1029, "JLQCD/TWQCD 09"       , ['s','w','r',0,tpos]],
 [0.600, 0.052,0.052, 0.0520,0.0520, "Gulpers 15"           , ['s','w','g',0,tpos]]
]

datphen=[
 [0.610, 0.040,0.040, 0.0400,0.0400, "Colangelo 01"         , ['o','b','b',0,tpos]]
]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) # is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN" with respect to the above
	      # there is another column at the end specifying the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [np.nan,0.000,0.000,0.000,0.000, "placeholder"                   , ['s','w','w',0,tpos],0],
#[np.nan,0.000,0.000,0.000,0.000, "placeholder"                   , ['s','w','w',0,tpos],0],
 [np.nan,0.000,0.000,0.000,0.000, "placeholder"                   , ['s','k','k',0,tpos],0],
 [np.nan,0.000,0.000,0.000,0.000, "placeholder"                   , ['s','w','w',0,tpos],0]
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datphen,dat2,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker


# FLAG plot for l4bar
# check whether numpy library is present
try:
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$\\bar{\\ell}_4$"			# plot title
plotnamestring	= "su2_l4bar"			# filename for plot
plotxticks	= ([[3.5, 4, 4.5, 5, 5.5]])      # locations for x-ticks
yaxisstrings	= [				# x,y-location for y-strings
		   [2.8,28.6,"$\\rm N_f=2+1+1$"],
		   [2.8,18.5,"$\\rm N_f=2+1$"],
		   [2.8,07.5,"$\\rm N_f=2$"]
		    ]
xlimits		= [3,7.5]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 6					# x-position for the data labels


# the following blocks contain the list of
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1,
# respectively
#
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
 [4.67,0.03,0.03,0.10,0.10,"ETM 10",                     ['s','w','r',0,tpos]],
 [4.73,0.02,0.02,0.10,0.10,"ETM 11",                     ['s','g','g',0,tpos]]  # update to and systematic error taken from ETM 10
]

dat2p1=[
 [4.43 ,0.14 ,0.14 ,0.77 ,0.77 , "RBC/UKQCD 08",         ['s','w','r',0,tpos]],
#[4.21 ,0.11 ,0.11 ,0.11 ,0.11 , "PACS-CS 08",           ['s','w','r',0,tpos]],
 [3.9  ,0.2  ,0.2  ,0.36 ,0.36 , "MILC 09A, $SU(2)$-fit",['s','l','g',0,tpos]],
 [4.03 ,0.16 ,0.16 ,0.23 ,0.23 , "MILC 09A, $SU(3)$-fit",['s','l','g',0,tpos]],
#[3.83 ,0.09 ,0.09 ,0.39 ,0.39 , "RBC/UKQCD 10A",        ['s','g','g',0,tpos]], # where does this syst error come from ?
 [3.98 ,0.32 ,0.32 ,0.43 ,0.60 , "MILC 10A",             ['s','l','g',0,tpos]],
 [4.29 ,0.21 ,0.21 ,0.837,0.837, "MILC 10",              ['s','g','g',0,tpos]],
 [4.30 ,0.51 ,0.51 ,0.79 ,0.98 , "NPLQCD 11",            ['s','g','g',0,tpos]],
 [4.03 ,0.03 ,0.03 ,0.16 ,0.16 , "Borsanyi 12",          ['s','g','g',0,tpos]],
 [3.99 ,0.16 ,0.16 ,0.184,0.184, "RBC/UKQCD 12",         ['s','l','g',0,tpos]],
 [3.80 ,0.40 ,0.40 ,0.447,0.447, "BMW 13",               ['s','g','g',0,tpos]],
 [4.113,0.059,0.059,0.059,0.059, "RBC/UKQCD 14B",        ['s','l','g',0,tpos]],
 [4.02 ,0.08 ,0.08 ,0.253,0.253, "RBC/UKQCD 15E",        ['s','g','g',0,tpos]]
]

dat2=[
 [4.12 ,0.35 ,0.35 ,0.46 ,0.55 , "JLQCD/TWQCD 08A",      ['s','w','r',0,tpos]],
 [4.4  ,0.2  ,0.2  ,0.22 ,0.22 , "ETM 08",               ['D','g','g',0,tpos]],
 [4.09 ,0.50 ,0.50 ,0.72 ,0.72 , "JLQCD/TWQCD 09",       ['D','w','r',0,tpos]],
 [4.66 ,0.04 ,0.04 ,0.33 ,0.04 , "ETM 09C",              ['s','g','g',0,tpos]],
 [4.582,0.017,0.017,0.026,0.026, "TWQCD 11",             ['s','w','r',0,tpos]],
 [4.56 ,0.10 ,0.10 ,0.11 ,0.11 , "Bernardoni 11",        ['s','w','r',0,tpos]],
#[4.2  ,0.1  ,0.1  ,0.00 ,0.00 , "QCDSF 13",             ['s','w','r',0,tpos]], # no systematic error
 [4.7  ,0.4  ,0.4  ,0.41 ,0.41 , "Brandt 13",            ['D','g','g',0,tpos]],
#[4.76 ,0.13 ,0.13 ,0.13 ,0.13 , "Gulpers 13",           ['D','w','r',0,tpos]], # no systematic error
 [4.54 ,0.30 ,0.30 ,0.30 ,0.30 , "Gulpers 15",           ['D','g','g',0,tpos]]
]

datphen=[
 [4.3, 0.9, 0.9, 0.9, 0.9,       "Gasser 84",            ['o','b','b',0,tpos]],
 [4.4, 0.2, 0.2, 0.2, 0.2,       "Colangelo 01",         ['o','b','b',0,tpos]]
]

# The color coding for the FLAG-average (below) is as for the data itself;
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more
# than one) # is significant, the last item will be plotted last and will
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN" with respect to the above
	      # there is another column at the end specifying the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
 [np.nan,0.000,0.000,0.000,0.000, "placeholder pheno",                 ['s','k','k',0,tpos],0],
 [ 4.403,0.280,0.280,0.138,0.138, "FLAG estimate for $\\rm N_f=2$",    ['s','k','k',0,tpos],0],
 [ 4.019,0.447,0.447,0.128,0.128, "FLAG estimate for $\\rm N_f=2+1$",  ['s','k','k',0,tpos],0],
 [ 4.730,0.100,0.100,0.100,0.100, "FLAG average for $\\rm N_f=2+1+1$", ['s','k','k',0,tpos],0]
]

### ETM 09C: [4.66,0.34,0.04] --> [4.66-0.5*0.15,0.34-0.5*0.15,0.04+1.5*0.15]=[4.585,0.265,0.265]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[datphen,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################

# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current
# working directory. That's where all plots will be stored
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker


\newpage
\subsection{Notes to section \ref{sec:LECs} on Low-Energy Constants}

\enlargethispage{20ex}

\begin{table}[!htp]
{\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} c c l l}
\hline\hline
\\[-1.2ex]
Collab. & Ref. & $\Nf$ & $a$ [fm]& Description
\\[1.0ex]
\hline \hline
\\[-1.2ex]
%
Cichy 16tyj & \cite{Cichy:2016tyj} & 2+1+1 & 0.0815(30) & \parbox[t]{7.5cm}{One ensemble of arXiv:1403.4504}\\
[1.0ex] \hline \\[-1.2ex]%
%
%HPQCD 13A, 15B  & \cite{Koponen:2015tkr} & 2+1+1 & 0.09--0.15 & \parbox[t]{7.5cm}{Configurations are shared with HPQCD13A.}\\
%[1.0ex] \hline \\[-1.2ex]%
%
HPQCD 13A, 15B  & \cite{Dowdall:2013rya,Koponen:2015tkr} & 2+1+1 & 0.09--0.15 & \parbox[t]{5.5cm}{Configurations are shared with MILC.}\\
[1.0ex] \hline \\[-1.2ex]%
%
%ETM 13 & \cite{Cichy:2013gja} & 2+1+1 & 0.0607--0.0863 & \parbox[t]{7.5cm}{Configurations are shared with ETM 11.}\\
%[1.0ex] \hline \\[-1.2ex]%
%
ETM 11, 13 & \cite{Baron:2011sf,Cichy:2013gja} & 2+1+1 & 0.0607--0.0863 & \parbox[t]{5.5cm}{3 lattice spacings fixed through $F_\pi/M_\pi$.}\\
[1.0ex] \hline\\[-1.2ex]
%
ETM 10 & \cite{Baron:2010bv} & 2+1+1 & 0.078, 0.086 & \parbox[t]{5.5cm}{Fixed through $F_\pi/M_\pi$.}
\\[1.0ex]
\hline
\\[-1.2ex]
%
JLQCD 16eqs  & \cite{Cossu:2016eqs} & 2+1 & 0.04--0.08 & \parbox[t]{5.5cm}{
Fixed through $\sqrt{t_0}=0.1465(21)(13)$ fm.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
%
JLQCD 15A  & \cite{Aoki:2015pba} & 2+1 & 0.112 & \parbox[t]{5.5cm}{Fixed through $\Omega$ baryon mass}\\
[1.0ex] \hline \\[-1.2ex]%
%
%RBC/UKQCD 14B, 15E & \cite{Boyle:2015exm} & 2+1 & $a^{-1}=0.981$...2.3820  & Confs shared with RBC/UKQCD 14B RBC/UKQCD 12.\\
%[1.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 14B, 15E & \cite{Blum:2014tka,Boyle:2015exm} & 2+1 & $a^{-1}=$1.730--3.148  & Fixed through $m_\pi$, $m_K$, and $m_{\Omega}$. \\
[1.0ex] \hline  \\[-1.2ex]
%
%JLQCD 14   & \cite{Fukaya:2014jka} & 2+1 & $a^{-1}=$1.759  & Confs shared with JLQCD/TWQCD 10A.\\
%[1.0ex] \hline  \\[-1.2ex]
%
Boyle 14 & \cite{Boyle:2014pja} & 2+1 & $a^{-1}=$1.37, 2.31  & Shared with RBC/UKQCD 12.\\
[1.0ex] \hline  \\[-1.2ex]
%
BMW 13 & \cite{Durr:2013goa} & 2+1 & 0.054--0.093  & Scale set through $\Omega$ baryon mass.\\
[1.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 12 & \cite{Arthur:2012opa} & 2+1 & \parbox[t]{2.5cm}{0.086, 0.114 and 0.144 for $M_\pi^{\mr{min}}$} & Scale set through $m_\Omega$.\\
[6.0ex] \hline  \\[-1.2ex]
%
 Borsanyi 12 & \cite{Borsanyi:2012zv} & 2+1 & 0.097--0.284 & Scale fixed through $F_\pi/M_\pi$.\\
[1.0ex] \hline  \\[-1.2ex]
%
 NPLQCD 11 & \cite{Beane:2011zm} & 2+1 & 0.09, 0.125 & \parbox[t]{5.5cm}{Configurations are shared with MILC 09 \cite{Bazavov:2009bb}.}\\
[4.0ex] \hline  \\[-1.2ex]
%
 \parbox[t]{3cm}{MILC 09, 09A, 10, 10A} & \parbox[t]{1.5cm}{\cite{Bazavov:2009bb,Bazavov:2009fk, Bazavov:2010hj,Bazavov:2010yq}} & 2+1 & 0.045--0.18 & \parbox[t]{5.5cm}{3 lattice
spacings, continuum extrapolation by means of RS{\Ch}PT.}\\
[4.0ex] \hline  \\[-1.2ex]
%
\parbox[t]{3cm}{JLQCD(/TWQCD) 08B, 09, 10A, 14  } & \parbox[t]{1.5cm}{\cite{Chiu:2008kt, Fukaya:2009fh,Fukaya:2010na, Fukaya:2014jka}} & 2+1, 3 & 0.11 & \parbox[t]{5.5cm}{One lattice spacing, fixed through $m_\Omega$.}
  \\
[4.0ex] \hline\\[-1.2ex]
%
RBC/UKQCD 09, 10A & \cite{Boyle:2009xi, Aoki:2010dy} & 2+1 & \parbox[t]{1.7cm}{0.1106(27), 0.0888(12)} & \parbox[t]{5.5cm}{Two lattice spacings. Data combined in global chiral-continuum fits.}
  \\
[4.0ex] \hline\\[-1.2ex]
%
%JLQCD 09 & \cite{Fukaya:2009fh} & 2+1 & 0.1075(7) &  \parbox[t]{7.0cm}{Scale fixed through $r_0$.
%Systematic error associated to lattice artifacts of
% order 7.4\% estimated through mismatch of the lattice spacing obtained from different inputs.
%} \\
%[1.0ex] \hline \\[-1.2ex]
%
%MILC 09, 09A   & \cite{Bazavov:2009bb,Bazavov:2009fk}  & 2+1 & 0.045--0.18 & \parbox[t]{7.0cm}
% {Total of 6 lattice spacings, continuum extrapolation by means of RS{\Ch}PT.} \\
%[3.0ex] \hline \\[-1.2ex]
%
TWQCD 08 & \cite{Chiu:2008jq} & 2+1 & 0.122(3) & \parbox[t]{5.5cm}{Scale fixed through $m_\rho$, $r_0$. %No estimate of systematic effects.
}  \\
[1.0ex] \hline\\[-1.2ex]
%
%JLQCD/TWQCD 08B & \cite{Chiu:2008kt} & 2+1 &  0.1075(7) & \parbox[t]{7.0cm}{Scale fixed through $r_0$. %No estimate of systematic effects for the observable adopted in this work.
%}   \\
%[1.0ex] \hline \\[-1.2ex]
%
PACS-CS 08, 11A   & \cite{Aoki:2008sm, Nguyen:2011ek}     & 2+1 & 0.0907       & \parbox[t]{5.5cm}
 {One lattice spacing.
%, no attempt to estimate size of cut-off effects.
} \\
[1.0ex] \hline \\[-1.2ex]
%
RBC/UKQCD 08A, 08 & \cite{Boyle:2008yd,Allton:2008pn}   & 2+1 & 0.114        & \parbox[t]{5.5cm}
 {One lattice spacing, attempt to estimate cut-off effects via
 formal argument.} \\
[6.0ex]\hline \\[-1.2ex]
%
%RBC/UKQCD 08A & \cite{Boyle:2008yd} & 2+1 &0.114 & \parbox[t]{7.0cm}
% {Only one lattice spacing, attempt to estimate size of cut-off effects via
% formal argument.} \\
%[3.0ex]\hline \\[-1.2ex]
%
NPLQCD 06 & \cite{Beane:2006kx} & 2+1 & 0.125 & \parbox[t]{5.5cm}{One lattice spacing, continuum {\Ch}PT used.} \\
[4.0ex]\hline \\[-1.2ex]
%
LHP 04 & \cite{Bonnet:2004fr} & 2+1 & $\simeq 0.12 $ & \parbox[t]{5.5cm}{Only one lattice spacing, mixed discretization approach.}\\
[4.0ex]\hline \hline \\[-1.2ex]
%
\end{tabular*}
\caption{Continuum extrapolations/estimation of lattice artifacts in
  $\Nf=2+1+1$ and $2+1$ determinations of the Low-Energy Constants.}
}
\end{table}

\clearpage

\begin{table}[!htp]
{\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} c c l l}
\hline\hline \\[-1.2ex]
Collab. & Ref. & $\Nf$ & $a$ [fm]& Description
\\[1.0ex] \hline \hline \\[-1.2ex]
%
ETMC 15A& \cite{Abdel-Rehim:2015pwa} & 2 & 0.0914(3)(15) & \parbox[t]{5.5cm}{Weighted average using $m_\pi$, $f_\pi$, $f_K$, $m_N$.
}\\
[4.0ex] \hline \\[-1.2ex]%
%
G\"ulpers 15& \cite{Gulpers:2015bba} & 2 & 0.050, 0.063, 0.079 & \parbox[t]{5.5cm}{Scale fixed through $m_{\Omega}$.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
Engel 14& \cite{Engel:2014eea} & 2 & \parbox[t]{3.0cm}{0.0483(4), 0.0652(6), 0.0749(8)}& \parbox[t]{5.5cm}{Scale fixed through $F_K$.
}\\
[4.0ex] \hline \\[-1.2ex]%
%
G\"ulpers 13& \cite{Gulpers:2013uca} & 2 & 0.063 & \parbox[t]{5.5cm}{Scale fixed through $m_{\Omega}$.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
Brandt 13& \cite{Brandt:2013dua} & 2 & 0.05--0.08 & \parbox[t]{5.5cm}{Configurations are shared with CLS.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
QCDSF 13& \cite{Horsley:2013ayv} & 2 & 0.06--0.076 & \parbox[t]{5.5cm}{Scale fixed through $r_0 = 0.50(1)$ fm.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
%ETM 13& \cite{Cichy:2013gja} & 2 & 0.05--0.1 & \parbox[t]{7.5cm}{Configurations are shared with ETM 09C.
%}\\
%[1.0ex] \hline \\[-1.2ex]%
%%
%ETM 12 & \cite{Burger:2012ti} & 2 & 0.05--0.1 & \parbox[t]{7.5cm}{Configurations are shared with ETM 09C.
%}\\
%[1.0ex] \hline \\[-1.2ex]
%%
Bernardoni 11 & \cite{Bernardoni:2011kd} & 2 & 0.0649(10) & \parbox[t]{5.5cm}{Configurations are shared with CLS.
}\\
[1.0ex] \hline \\[-1.2ex]
%
TWQCD 11A, 11 & \cite{Chiu:2011dz,Chiu:2011bm} & 2 & 0.1034(1)(2) & \parbox[t]{7.5cm}
{
Scale fixed through $r_0$.
}\\
[1.0ex] \hline \\[-1.2ex]
%
%TWQCD 11A & \cite{Chiu:2011dz} & 2 & 0.1032(2) & \parbox[t]{7.5cm}{
%Scale fixed through $r_0$.
%}\\
%[1.0ex] \hline \\[-1.2ex]
%
Bernardoni 10 & \cite{Bernardoni:2010nf} & 2 & 0.0784(10) & \parbox[t]{5.5cm}{Scale fixed through $M_K$. Non-perturbative $\cO(a)$ improvement.  No estimate of systematic error. }\\
[7.0ex] \hline \\[-1.2ex]
%
%JLQCD/TWQCD 09, 10A & \cite{Fukaya:2010na} & 2 & 0.11
%& \parbox[t]{7.5cm}{One lattice spacing fixed
%through $r_0$.}
% \\
%[1.0ex] \hline  \\[-1.2ex]
%
%JLQCD/TWQCD 09 & \cite{JLQCD:2009qn} & 2 & 0.1184(21) & \parbox[t]{7.5cm}{Automatic $\cO(a)$ impr., exact chiral symmetry. Scale fixed through $r_0$.}\\
%[6.0ex] \hline  \\[-1.2ex]
%
ETM 09B & \cite{Jansen:2009tt} & 2 & 0.063, 0.073 & \parbox[t]{5.5cm}{Automatic $\cO(a)$ impr.
%Estimate of lattice artifacts ($4\%-10\%$ effect, not added in final result) %by comparing two lattice spacings.
$r_0=0.49$\,fm used.}\\ %to convert results in physical units.}.\\
[1.0ex] \hline  \\[-1.2ex]
%
ETM 09C, 12, 13 & \parbox[t]{1.5
  cm}{\cite{Baron:2009wt,Burger:2012ti,Cichy:2013gja}}  & 2 &  0.051--0.1 & \parbox[t]{5.5cm}{Automatic $\cO(a)$ impr. Scale fixed through $F_\pi$. 4 lattice spacings, continuum extrapolation.}\\
[7.0ex] \hline \\[-1.2ex]
%
ETM 08 & \cite{Frezzotti:2008dr} & 2 & 0.07-0.09             & \parbox[t]{5.5cm}{Automatic $\cO(a)$ impr. Two lattice spacings. Scale fixed through $F_\pi$.   }              \\
[4.0ex] \hline \\[-1.2ex]
%
\parbox[t]{3.5 cm}{{JLQCD/TWQCD 07},\\ 07A, 08A, 09, 10A \\JLQCD 08A} & \parbox[t]{1.7
  cm}{\cite{Aoki:2007pw, Fukaya:2007pn, Noaki:2008iy, JLQCD:2009qn, Fukaya:2010na},
\cite{ Shintani:2008qe}} & 2 & 0.1184(3)(21)
& \parbox[t]{5.5cm}{Automatic $\cO(a)$ impr., exact chiral symmetry. Scale fixed through $r_0$. %Scale ambiguity added as systematic error.
}\\
[7.0ex] \hline \\[-1.2ex]
%
CERN 08 & \cite{Giusti:2008vb} & 2 & 0.0784(10) & \parbox[t]{5.5cm}{Scale fixed through $M_K$. Non-perturbative $\cO(a)$ improvement.
%No estimate of systematic error.
}\\
[4.0ex] \hline \\[-1.2ex]
%
Hasenfratz 08 & \cite{Hasenfratz:2008ce} & 2 & 0.1153(5) & \parbox[t]{5.5cm}{Tree level $\cO(a)$
improvement. Scale fixed through $r_0$. Estimate of lattice artifacts via W{\Ch}PT \cite{Bar:2008th}.}\\
[7.0ex] \hline \\[-1.2ex]
%
 CERN-TOV 06 & \cite{DelDebbio:2006cn} & 2 & \parbox[t]{1.0cm}{0.0717(15), 0.0521(7),  0.0784(10)} &\parbox[t]{5.5cm}{Scale fixed through $M_K$. The lattice with $a=0.0784(10)$ is obtained with non-perturbative $\cO(a)$ improvement.
}\\
[7.0ex] \hline \\[-1.2ex]
%
QCDSF/UKQCD 06A & \cite{Brommel:2006ww} & 2& 0.07-0.115 &  \parbox[t]{5.5cm}{5 lattice spacings. Non-perturbative $\cO(a)$ improvement. Scale fixed through $r_0$.}\\
[7.0ex] \hline \hline \\[-1.2ex]
\end{tabular*}
\caption{Continuum extrapolations/estimation of lattice artifacts in $\Nf=2$
  determinations of the Low-Energy Constants.}
}
\end{table}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{table}[!htp]
{\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} c c c l}
\hline\hline \\[-1.2ex]
Collab. & Ref. & $\Nf$ & $M_{\pi,\text{min}}$ [MeV] & Description
\\[1.0ex] \hline \hline \\[-1.2ex]
%
Cichy 16tyj & \cite{Cichy:2016tyj} & 2+1+1 & 370 or 350 & \parbox[t]{7.5cm}{370 (paper) versus 350 (B.50.32 of 1403.4504)}\\
[1.0ex] \hline \\[-1.2ex]%
%
HPQCD 15B  & \cite{Koponen:2015tkr} & 2+1+1 & 175 & \parbox[t]{5.2cm}{Simulated at physical point.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
HPQCD 13A& \cite{Dowdall:2013rya} & 2+1+1 & 175 & \parbox[t]{5.2cm}{NLO chiral fit.
}\\
[1.0ex] \hline \\[-1.2ex]%
%%
ETM 13& \cite{Cichy:2013gja} & 2+1+1 & 270 & \parbox[t]{5.2cm}{Linear fit in the quark mass.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
ETM 11 & \cite{Baron:2011sf} & 2+1+1 & 270 & \parbox[t]{5.2cm}{NLO $SU(2)$ chiral fit.}\\
[1.0ex] \hline\\[-1.2ex]
%
ETM 10 & \cite{Baron:2010bv} & 2+1+1 &  $270$ & \parbox[t]{5.2cm}{$SU(2)$ NLO and NNLO fits. }\\
[1.0ex] \hline \hline
%
\end{tabular*}
\caption{Chiral extrapolation/minimum pion mass in $\Nf=2+1+1$ 
  determinations of the Low-Energy Constants.}
}
\end{table}


\begin{table}[!htp]
{\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} c c c l}
\hline\hline \\[-1.2ex]
Collab. & Ref. & $\Nf$ & $M_{\pi,\text{min}}$ [MeV] & Description
\\[1.0ex] \hline \hline \\[-1.2ex]
%
JLQCD 16eqs & \cite{Cossu:2016eqs} & 2+1 & 225.8(0.3)  & \parbox[t]{5.2cm}{NLO $SU(2)$ ChPT}\\
[1.0ex] \hline  \\[-1.2ex]
%
%
RBC/UKQCD 15E & \cite{Boyle:2015exm} & 2+1 & 117.3(4.4)  & \parbox[t]{5.2cm}{GMOR for $\Sigma$, NNLO PQ $SU(2)$ $\chi$PT.}\\
[4.0ex] \hline  \\[-1.2ex]
%
JLQCD 15A  & \cite{Aoki:2015pba} & 2+1 & 290  & \parbox[t]{5.9cm}{Dynamical overlap, NNLO $SU(3)$.}\\
[1.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 14B & \cite{Blum:2014tka} & 2+1 & 139.2  & \parbox[t]{5.9cm}{GMOR for $\Sigma$, global cont./chiral fit.}\\
[1.0ex] \hline  \\[-1.2ex]
%
JLQCD 14   & \cite{Fukaya:2014jka} & 2+1 & 99  & \parbox[t]{5.9cm}{$\epsilon$ expansion.}\\
[1.0ex] \hline  \\[-1.2ex]
%
Boyle 14 & \cite{Boyle:2014pja} & 2+1 & 171  & \parbox[t]{5.9cm}{Combines latt/pheno.}\\
[1.0ex] \hline  \\[-1.2ex]
%
BMW 13 & \cite{Durr:2013goa} & 2+1 & 120 & \parbox[t]{5.2cm}{NLO and NNLO $SU(2)$ fits tested with x and $\xi$ expansion.}\\
[4.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 12 & \cite{Arthur:2012opa} & 2+1 & \parbox[t]{2.2cm}{293 plus run at 171, 246} & \parbox[t]{5.2cm}{NLO $SU(2)$ $\chi$PT incl.\ finite-V and some discr.\ effects} \\
[4.0ex] \hline  \\[-1.2ex]
%
Borsanyi 12 & \cite{Borsanyi:2012zv} & 2+1 & 135 &  NNLO $SU(2)$ chiral fit.\\
[1.0ex] \hline  \\[-1.2ex]
%
NPLQCD 11 & \cite{Beane:2011zm} & 2+1 & 235 & NNLO $SU(2)$ mixed action $\chi$PT.\\
[1.0ex] \hline  \\[-1.2ex]
%
PACS-CS 11A   & \cite{Nguyen:2011ek}     & 2+1 &  $296$ & \parbox[t]{5.2cm}
 {Additional test runs at physical point.} \\
[4.0ex] \hline \\[-1.2ex]
%
%
%MILC 10, 10A       & \cite{Bazavov:2010hj,Bazavov:2010yq}  & 2+1 &     &  Cf.~MILC 09A. \\
%[1.0ex] \hline \\[-1.2ex]
%
JLQCD/TWQCD 09, 10A & \cite{Fukaya:2010na} & 2+1,3 &
\parbox[t]{1.8cm}{ 100($\epsilon$-reg.),\\ 290($p$-reg.)}
%\parbox[t]{2.8cm}{$m_{ud}\Sigma V <1$ ($\epsilon$-reg)\\ $m_{ud}= m_s/6-m_s$ ($p$-reg.), \\ $m_s\sim$ phys.}
& \parbox[t]{5.2cm}{$N_f=2+1$ runs both in $\epsilon$- and $p$-regime; $N_f=3$ runs only in $p$-regime. NLO $\chi$PT fit of the spectral density interpolating the two regimes.}
%$\binom{+2.2\%}{-0.2\%}$ systematic error associated to $\Sigma^{1/3}$ and $\pm 8.7\%$ to $\Sigma_0^{1/3}$.
\\
[12.0ex] \hline\\[-1.2ex]
%
RBC/UKQCD 09, 10A & \cite{Boyle:2009xi, Aoki:2010dy} & 2+1 & 290--420 & \parbox[t]{5.2cm}{Valence pions mass is 225-420 MeV. NLO $SU(2)$ $\chi$PT fit.} \\
[4.0ex] \hline\\[-1.2ex]
%
%JLQCD 09 & \cite{Fukaya:2009fh} & 2+1 & \parbox[t]{2.8cm}{$m_{ud}\Sigma V <1$ ($\epsilon$-reg)\\ $m_{ud}= m_s/6-m_s$ ($p$-reg.), \\ $m_s\sim$ phys.} &  \parbox[t]{5.9cm}{Quark masses both in the $\epsilon$ and in the $p$-regime.  NLO chiral fit of the spectral density interpolating the two regimes. $\binom{+2.2}{-0.7}$ \% systematic error estimated by varying the fit range and using formulae for SU(2) versus SU(3) {\Ch}PT.   }\\
%[13.0ex] \hline \\[-1.2ex]
%
MILC 09, 09A, 10, 10A      & \cite{Bazavov:2009bb,Bazavov:2009fk, Bazavov:2010hj,Bazavov:2010yq}  & 2+1 &  $258$ & \parbox[t]{5.2cm}
 {
%Value taken from text of \cite{Bazavov:2009bb}; according to \cite{Bazavov:2009fk}
 Lightest Nambu-Goldstone mass is 224\,MeV and
 lightest RMS mass is 258\,MeV (at 0.06\,fm). }\\
[7.0ex] \hline \\[-1.2ex]
%
%MILC 09A      & \cite{Bazavov:2009fk}  & 2+1 & 258 &  \parbox[t]{5.9cm}{Lightest
%Nambu-Goldstone mass is 177\,MeV (at 0.09\,fm) and
% lightest RMS mass is 258\,MeV (at 0.06\,fm).} \\
%[5.0ex] \hline \\[-1.2ex]
%
TWQCD 08 & \cite{Chiu:2008jq} & 2+1 &  \parbox[t]{2.2cm}{{$m_{ud}=m_s/4$},  $m_s\sim$ phys.}  & \parbox[t]{5.2cm}{Quark condensate extracted from topological susceptibility, LO chiral fit.}  \\
[6.0ex] \hline\\[-1.2ex]
%
%JLQCD/TWQCD 08B & \cite{Chiu:2008kt} & 2+1 & \parbox[t]{2.2cm}{$m_{ud}\geq m_s/6$, \\ $m_s\sim$ phys.}  &  \p%arbox[t]{5.9cm}{Quark condensate extracted from topological susceptibility, LO chiral fit.} \\
%[3.0ex] \hline \\[-1.2ex]
%
PACS-CS 08   & \cite{Aoki:2008sm}     & 2+1 &  $156$ & \parbox[t]{5.9cm}
 {Simulation at physical point.} \\
[1.0ex] \hline \\[-1.2ex]
%
RBC/UKQCD 08 & \cite{Allton:2008pn}   & 2+1 &  $330$ & \parbox[t]{5.2cm}
 {%Other unitary masses are 419, 557, 672 MeV,
 Lightest velence pion mass is $242\MeV$.} \\
[4.0ex] \hline \\[-1.2ex]
%
RBC/UKQCD 08A & \cite{Boyle:2008yd} & 2+1 & 330 & \parbox[t]{5.9cm}{Computed at one pion mass.
%Low values of $q^2$ reached thanks to partially twisted boundary conditions. LECs determined from NLO $\chi$PT fit in $q^2$.
}\\
[1.0ex] \hline \\[-1.2ex]
%
NPLQCD 06 & \cite{Beane:2006kx} & 2+1 & $460$  & \parbox[t]{5.2cm}{Value refers to lightest RMS mass at $a=0.125$\,fm as quoted in \cite{Bazavov:2009fk}.} \\
[4.0ex]\hline \\[-1.2ex]
%
LHP 04 & \cite{Bonnet:2004fr} & 2+1 &  $318$ &  \parbox[t]{5.9cm}{Vector meson dominance fit.}\\
[1.0ex]\hline \hline \\[-1.2ex]
%
\end{tabular*}
\caption{Chiral extrapolation/minimum pion mass in $2+1$
  determinations of the Low-Energy Constants.}
}
\end{table}

\clearpage

\begin{table}[!htp]
{\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} c c c l}
\hline\hline
\\[-1.2ex]
%
Collab. & Ref. & $\Nf$ & $M_{\pi,\text{min}}$ [MeV] & Description \\
[1.0ex] \hline \hline \\[-1.2ex]
%
ETMC 15A& \cite{Abdel-Rehim:2015pwa} & 2 & 134 & \parbox[t]{7.5cm}{Simulation at physical point.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
G\"ulpers 15& \cite{Gulpers:2015bba} & 2 & 193 & \parbox[t]{7.5cm}{NLO $SU(2)$ fit.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
Engel 14& \cite{Engel:2014eea} & 2 & 193 & \parbox[t]{5.5cm}{NLO $SU(2)$ fit, Dirac op. and GMOR for $\Sigma$.
}\\
[4.0ex] \hline \\[-1.2ex]%
%
G\"ulpers 13& \cite{Gulpers:2013uca} & 2 & 280 & \parbox[t]{7.5cm}{NLO $\chi$PT fit.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
Brandt 13& \cite{Brandt:2013dua} & 2 & 280 & \parbox[t]{7.5cm}{Configurations are shared with CLS.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
QCDSF 13& \cite{Horsley:2013ayv} & 2& 130 & \parbox[t]{7.5cm}{Fit with $\chi$PT + analytic.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
ETM 12, 13& \cite{Burger:2012ti, Cichy:2013gja} & 2 & 260 & \parbox[t]{7.5cm}{Confs shared with ETM 09C.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
%ETM 12 & \cite{Burger:2012ti} & 2 & 260 & \parbox[t]{7.5cm}{Configurations are shared with ETM 09C.
%}\\
%[1.0ex] \hline \\[-1.2ex]
%
Bernardoni 11 & \cite{Bernardoni:2011kd} & 2 & 312 & \parbox[t]{5.5cm}{Overlap varence + $\cO(a)$ improved Wilson sea, mixed regime $\chi$PT.}
\\
[4.0ex] \hline \\[-1.2ex]
%
TWQCD 11 & \cite{Chiu:2011bm} & 2 & 230 & \parbox[t]{5.5cm}
{
NLO $SU(2)$ $\chi$PT fit.
}\\
[1.0ex] \hline \\[-1.2ex]
%
TWQCD 11A & \cite{Chiu:2011dz} & 2 & 220 & \parbox[t]{5.5cm}{
NLO $\chi$PT. %(infinite $V$) for topological susceptibility $\chi_{\rm top}$.
}\\
[1.0ex] \hline \\[-1.2ex]
%
Bernardoni 10 & \cite{Bernardoni:2010nf}&  2& 297, 377, 426 & \parbox[t]{5.5cm}{NLO $SU(2)$ fit of $\chi_{\rm top}$.}\\
[1.0ex] \hline \\[-2.0ex]
%
JLQCD/TWQCD 10A & \cite{Fukaya:2010na} & 2 &  \parbox[t]{3cm}{$\sqrt{2m_{\rm min}\Sigma}/F$=120 ($\epsilon$-reg.), 290 ($p$-reg.)}
%\parbox[t]{3.5cm}{$m\Sigma V< 1$ ($\epsilon$-reg.)
%\\ $m\simeq m_s/6-m_s$ ($p$-reg.)}
& \parbox[t]{5.5cm}{Data both in the $p$ and $\epsilon$-regime. NLO chiral fit of the spectral density interpolating the two regimes.
%$\binom{+1.2\%}{-1.8\%}$ for $\Sigma^{1/3}$ associated to chiral fit.
} \\
[7.0ex]\hline \\[-2.0ex]
%
JLQCD/TWQCD 09 & \cite{JLQCD:2009qn} & 2 & 290  &  \parbox[t]{5.5cm}{LECs extracted from NNLO chiral fit of vector and scalar radii $\langle r^2\rangle^\pi_{V,S}$.}\\
[4.0ex]\hline \\[-1.2ex]
%
ETM 09B & \cite{Jansen:2009tt} & 2 & \parbox{3.5cm}{
$\sqrt{2m_{\rm min}\Sigma}/F$=85%$m\Sigma V\simeq 0.11-0.35$
} &  \parbox[t]{5.5cm}{NLO $SU(2)$ $\epsilon$-regime fit.}\\
[1.0ex]\hline \\[-1.2ex]
%
ETM 09C & \cite{Baron:2009wt} & 2 & $280$    & \parbox[t]{5.5cm}{NNLO $SU(2)$ fit. %Systematic error estimated by performing a large set of different fits.
} \\
[1.0ex] \hline \\[-1.2ex]
%
ETM 08  & \cite{Frezzotti:2008dr} & 2 &   $260$  &   \parbox[t]{5.5cm}{From pion form factor using NNLO {\Ch}PT and exp. value of $\langle r^2\rangle^\pi_{S}$.   }        \\
[4.0ex] \hline \\[-1.2ex]
%
\parbox[t]{3.5 cm}{JLQCD/TWQCD 08A \\JLQCD 08A} & \parbox[t]{0.7
  cm}{\cite{Noaki:2008iy} \\ \cite{Shintani:2008qe}} & 2 & 290 &
\parbox[t]{5.5cm}{NNLO $SU(2)$ fit.
% up to $M_\pi=$ 750 MeV. Estimate of
%  systematic error due to truncation of chiral series.
}\\
[4.0ex] \hline \\[-1.2ex]
%
CERN 08 & \cite{Giusti:2008vb} &  2 & $m_{q,\text{min}}$=13 MeV & \parbox[t]{5.5cm}{NLO $SU(2)$ fit for the mode number.% of the Dirac operator.
}\\
[1.0ex] \hline \\[-1.2ex]
%
Hasenfratz 08 & \cite{Hasenfratz:2008ce} & 2 &  \parbox[t]{3.5cm}{$\sqrt{2m_{\rm min}\Sigma}/F$=220
%$\mu_1=m\Sigma
%V_1\simeq 0.7-2.9$\\ $\mu_2=m\Sigma V_2\simeq 2.1-5.0$
}
&  NLO $SU(2)$ $\epsilon$-regime fit.
%\parbox[t]{5.5cm}{$\epsilon$-regime: higher values might be too large to
%  be considered in the $\epsilon$-regime.  NLO SU(2) $\epsilon$-regime fit.}
\\
[1.0ex] \hline \\[-1.2ex]
%
JLQCD/TWQCD 07  & \cite{Fukaya:2007pn} & 2 & $\sqrt{2m_{\rm min}\Sigma}/F$=120 & \parbox[t]{5.5cm}{NLO $SU(2)$ $\epsilon$-regime fit.} \\
[1.0ex]\hline \\[-1.2ex]
%
JLQCD/TWQCD 07A & \cite{Aoki:2007pw} & 2 & $m_{ud}= m_s/6-m_s$ & \parbox[t]{5.5cm}{$\Sigma$ from $\chi_{t}$, LO chiral fit.}\\
[1.0ex] \hline \\[-1.2ex]
%
 CERN-TOV 06 & \cite{DelDebbio:2006cn} & 2 & 403, 381, 377 & \parbox[t]{5.5cm}{NLO $SU(2)$ fit.% up to $M_\pi\simeq $ 540 MeV.
}\\
[1.0ex] \hline \\[-1.2ex]
%
QCDSF/UKQCD 06A & \cite{Brommel:2006ww} & 2 & 400 & \parbox[t]{5.5cm}{Several fit functions to extrapolate the pion form factor.} \\
[4.0ex] \hline \hline \\[-1.2ex]
\end{tabular*}
\caption{Chiral extrapolation/minimum pion mass in $\Nf=2$ determinations of the Low-Energy Constants.}
}
\end{table}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{table}[!htp]
{\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} c c c c l}
\hline\hline \\[-1.2ex]
Collab.\ & Ref.\ & $\Nf$ & $L$ [fm] & $M_{\pi,\text{min}}L$ & Description
\\[1.0ex] \hline \hline \\[-1.2ex]
%
HPQCD 15B  & \cite{Koponen:2015tkr} & 2+1+1 & & 4.8 & \parbox[t]{7.5cm}{
}\\
[1.0ex] \hline \\[-1.2ex]%
%
HPQCD 13A& \cite{Dowdall:2013rya} & 2+1+1 & 4.8--5.5 & 3.3 & \parbox[t]{7.5cm}{
3 volumes are compared.
}\\
%
[1.0ex] \hline \\[-1.2ex]%
ETM 13& \cite{Cichy:2013gja} & 2+1+1 & 1.9--2.8 & 3.0 &\parbox[t]{7.5cm}{
4 volumes compared.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
%ETM 11 & \cite{Baron:2011sf} & 2+1+1 & 1.9--2.8 & 3.0 & See \cite{Baron:2010bv}.\\
%[1.0ex] \hline\\[-1.2ex]
%
ETM 10, 11 & \cite{Baron:2010bv,Baron:2011sf} & 2+1+1 & 1.9-2.8 & 3.0 &
\parbox[t]{4.25cm}{FSE estimate using \cite{Colangelo:2005gd}.
  %LECs are extrac\-ted from data which satisfy
  $M_{\pi^+} L\gtrsim 4$, but $M_{\pi^0} L \sim 2$. }\\
[4.0ex] \hline\\[-1.2ex]
%
%
%
%
%
JLQCD 16eqs  & \cite{Cossu:2016eqs} & 2+1 & & 4.1743 & 2 volumes.\\
[1.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 15E & \cite{Boyle:2015exm} & 2+1 & & 3.78  & 1 volume.\\
[1.0ex] \hline  \\[-1.2ex]
%
JLQCD 15A  & \cite{Aoki:2015pba} & 2+1 & & 3.88 & 1 volume.\\
[1.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 14B & \cite{Blum:2014tka} & 2+1 & & 5.476  & 1 volume.\\
[1.0ex] \hline  \\[-1.2ex]
%
JLQCD 14   & \cite{Fukaya:2014jka} & 2+1 & & 1.8  & $\epsilon$-regime\\
[1.0ex] \hline  \\[-1.2ex]
%
Boyle 14 & \cite{Boyle:2014pja} & 2+1 & & 4.6  & 1 volume.\\
[1.0ex] \hline  \\[-1.2ex]
%
BMW 13 & \cite{Durr:2013goa} & 2+1 & 2.1 & 3.0 & 3 volumes are compared.\\
[1.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 12 & \cite{Arthur:2012opa} & 2+1 & 2.7--4.6 & $>4$ & FSE seem to be very small. \\
[1.0ex] \hline  \\[-1.2ex]
%
 Borsanyi 12 & \cite{Borsanyi:2012zv} & 2+1 & 3.9 & 3.3 & Expected to be less than 1\%. \\
[1.0ex] \hline  \\[-1.2ex]
%
 NPLQCD 11 & \cite{Beane:2011zm} & 2+1 & 2.5--3.5 & 3.6 & Expected to be less than 1\%. \\
[1.0ex] \hline  \\[-1.2ex]
%
MILC 09, 09A, 10, 10A       & \cite{Bazavov:2009bb,Bazavov:2009fk, Bazavov:2010hj,Bazavov:2010yq}                 & 2+1 & 2.52 &
3.5--4.11  & \parbox[t]{4.8cm}{$L\!\geq\!2.9\,\fm$ for lighter masses.}\\
[1.0ex] \hline \\[-1.2ex]
%
JLQCD/TWQCD 09, 10A & \cite{Fukaya:2010na} & 2+1, 3 & 1.9, 2.7 &
& \parbox[t]{4.6cm}{
%Finite volume effects estimated by comparing with larger volume
2 volumes are compared for a fixed quark mass.
%For $F$ estimated to be
%  3\%, for $\Sigma^{1/3}$ and  $\Sigma_0^{1/3}$ $\binom{+0.3\%}{-1.0\%}$.} \\%.
}\\
[4.0ex]\hline\\[-1.2ex]
%
RBC/UKQCD 09, 10A & \cite{Boyle:2009xi, Aoki:2010dy} & 2+1 &  2.7   & $\simeq$ 4  & \parbox[t]{4.25cm}{
%Finite-volume corrections included by means of $\chi$PT
FSE estimated using $\chi$PT.} \\
[1.0ex] \hline\\[-1.2ex]
%
%JLQCD 09 & \cite{Fukaya:2009fh} & 2+1 & 1.72 &  $\epsilon/p$-regime   & \parbox[t]{4.25cm}{Finite-volume effects estimated by comparing with larger volume ($L=2.58$\,fm) for a fixed $m_{ud}$, $m_s$.}
%\\
%[9.0ex] \hline \\[-1.2ex]
%
%MILC 09, 09A  & \cite{Bazavov:2009bb,Bazavov:2009fk}  & 2+1 & 2.4/2.9 & 3.5/4.11 & \parbox[t]{4.8cm}
%{$L\!\geq\!2.9\,\fm$ for lighter masses.}
%; one coarse ensemble with $M_\pi L\!=\!3.5$, all fine ones have $M_\pi L\!>\!4.11$.
%\\
%[1.0ex] \hline \\[-1.2ex]
%
TWQCD 08 & \cite{Chiu:2008jq} & 2+1 &  1.95  &  - &  \parbox[t]{4.25cm}{No estimate of FSE.} \\
[1.0ex] \hline\\[-1.2ex]
%
%JLQCD/TWQCD 08B & \cite{Chiu:2008kt} & 2+1 &1.72  &  - &  \parbox[t]{4.25cm}{Fixing topoloical charge (to $\nu=0$) gives FSE \protect\cite{Aoki:2007ka}.} \\
%[3.0ex]\hline\\[-1.2ex]
%
PACS-CS 08, 11A   & \cite{Aoki:2008sm, Nguyen:2011ek}     & 2+1 & 2.9     & 2.3      & \parbox[t]{4.6cm}
 {FSE is the main concern of the authors. Additional test runs on $64^4$.} \\
[7.0ex] \hline \\[-1.2ex]
%
RBC/UKQCD 08 & \cite{Allton:2008pn}   & 2+1 & 2.74    & 4.6      & \parbox[t]{4.25cm}{FSE by means of {\Ch}PT.} \\
[1.0ex]\hline \\[-1.2ex]
%
RBC/UKQCD 08A & \cite{Boyle:2008yd} & 2+1 & 2.74 & 4.6 & \parbox[t]{4.25cm}{FSE estimated to be  $< 1 \%$. %using $\chi$PT.
}\\
[1.0ex] \hline \\[-1.2ex]
%
NPLQCD 06 & \cite{Beane:2006kx} & 2+1 &  2.5    & 3.7     & \parbox[t]{4.6cm}{Value refers to lightest valence pion mass.} \\
[4.0ex]\hline \\[-1.2ex]
%
 LHP 04 & \cite{Bonnet:2004fr} & 2+1 & $\simeq 2.4$ & 3.97 &  \parbox[t]{4.6cm}{Value refers to domain-wall valence pion mass.}\\
[4.0ex] \hline \hline\\[-1.2ex]
%
%
\end{tabular*}
\caption{Finite volume effects in $\Nf=2+1+1$ and $2+1$ determinations of the Low-Energy Constants.}
}
\end{table}

\clearpage

\begin{table}[!htp]
{\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} c c c c l}
\hline\hline \\[-1.2ex]
Collab.\ & Ref.\ & $\Nf$ & $L$ [fm] & $M_{\pi,\text{min}}L$ & Description
\\[1.0ex] \hline \hline \\[-1.2ex]
%
%
%
%
ETMC 15A& \cite{Abdel-Rehim:2015pwa} & 2 & & 4.39 & \parbox[t]{7.5cm}{ 2 volumes.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
G\"ulpers 15& \cite{Gulpers:2015bba} & 2 & &4.09 & \parbox[t]{7.5cm}{  3 volumes, CLS confs.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
Engel 14& \cite{Engel:2014eea} & 2 & & 4.2 & \parbox[t]{7.5cm}{ 3 volumes, CLS confs.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
G\"ulpers 13& \cite{Gulpers:2013uca}& 2 & 4--6 & 4.3 &  \parbox[t]{7.5cm}{Configs. shared with CLS.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
Brandt 13& \cite{Brandt:2013dua} & 2 & $\sim 5$ & 4 &  \parbox[t]{7.5cm}{Configs. shared with CLS.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
QCDSF 13& \cite{Horsley:2013ayv} & 2& 1.8--2.4 & 2.7 &  \parbox[t]{7.5cm}{ NLO $\chi$PT is used for FSE.
}\\
[1.0ex] \hline \\[-1.2ex]%
%
%ETM 13& \cite{Cichy:2013gja} & 2 & 2.0--2.5 & 3--4 & \parbox[t]{7.5cm}{Configs. shared with ETM 09C.
%}\\
%[1.0ex] \hline \\[-1.2ex]%
%%
%ETM 12 & \cite{Burger:2012ti} & 2 & 2.0--2.5 & 3--4 &\parbox[t]{7.5cm}{Configs. shared with ETM 09C.
%}\\
%[1.0ex] \hline \\[-1.2ex]
%
Bernardoni 11 & \cite{Bernardoni:2011kd} & 2 & 1.56 & 2.5  & \parbox[t]{4.2cm}{
Mixed regime $\chi$PT for FSE used.
}\\
[4.0ex] \hline \\[-1.2ex]
%
TWQCD 11 & \cite{Chiu:2011bm} & 2 & 1.65 & 1.92 & \parbox[t]{5.25cm}
{
$SU(2)$ $\chi$PT is used for FSE.
}\\
[1.0ex] \hline \\[-1.2ex]
%
TWQCD 11A & \cite{Chiu:2011dz} & 2 & 1.65 & 1.8 & \parbox[t]{5.25cm}{
No estimate of FSE.
}\\
[1.0ex] \hline \\[-1.2ex]
%
Bernardoni 10 & \cite{Bernardoni:2010nf} & 2 & 1.88 & 2.8 & \parbox[t]{4.2cm}{FSE included in the NLO chiral fit.}  \\
[4.0ex] \hline \\[-2.0ex]
%
JLQCD/TWQCD 10A & \cite{Fukaya:2010na} & 2 & 1.8-1.9 &  & \parbox[t]{4.2cm}{
FSE estimated from
%Finite volume effects
%estimated through differences between
different topological sectors.
%Systematic error of 3.7\% on $\Sigma^{1/3}$.
} \\
[4.0ex] \hline  \\[-1.2ex]
%
JLQCD/TWQCD 09 & \cite{JLQCD:2009qn} & 2 & 1.89 & 2.9
&  \parbox[t]{4.2cm}{%Finite-volume corrections for form factor
FSE by NLO  $\chi$PT,
%  \protect\cite{Bunton:2006va,Borasoy:2004zf}.
Additional FSE for fixing topology \protect\cite{Aoki:2007ka}.
%finite-volume effect for fixed topological charge \protect\cite{Aoki:2007ka}.
} \\
[6.0ex] \hline  \\[-2.0ex]
%
ETM 09B & \cite{Jansen:2009tt} & 2 & 1.3, 1.5 &  $\epsilon$-regime
&  \parbox[t]{4.2cm}{Topology: not fixed. %Comparison between
2 volumes.
%, estimate of $8\%$ systematic effect (not quoted in the final result). Volume might be too small for $\epsilon$-expansion.
}\\
[4.0ex] \hline  \\[-2.0ex]
%
ETM 09C, 12, 13 & \cite{Baron:2009wt,Burger:2012ti,Cichy:2013gja} & 2 & 2.0-2.5   & 3.2--4.4 %\parbox[t]{1.7cm}{4.4, 3.7, 3.2,\\ 3.5}
& \parbox[t]{4.2cm}{Several volumes. Finite-volume effects estimated through \protect\cite{Colangelo:2005gd}.}\\
[6.0ex] \hline \\[-2.0ex]
%
ETM 08 & \cite{Frezzotti:2008dr} & 2 & 2.1, 2.8  & 3.4, 3.7 &\parbox[t]{4.2cm}{
%For pion form factor
Only data with $M_\pi L\gtrsim 4$ are considered.
% in the final analysis. FSE for pion masses and decay constants are taken into account according to \cite{Colangelo:2005gd}
}\\
[4.0ex] \hline \\[-2.0ex]
%
\parbox[t]{3.5 cm}{JLQCD/TWQCD 08A \\JLQCD 08A} & \parbox[t]{0.7
  cm}{\cite{Noaki:2008iy} \\ \cite{Shintani:2008qe}} & 2 & 1.89 & 2.9
&  \parbox[t]{4.2cm}{%Finite-volume corrections
FSE estimates through
  \protect\cite{Colangelo:2005gd}. Additional FSE % finite-volume effect for
for fixing topology \protect\cite{Aoki:2007ka}.} \\
[6.0ex] \hline \\[-1.2ex]
%
CERN 08 & \cite{Giusti:2008vb}  & 2  & 1.88, 2.51  & - & \parbox[t]{5.25cm}{
%For the specific observable studied here the finite-volume effects $\propto e^{-M_\Lambda L/2}$, with $M_\Lambda \gg M_\pi$.
%Finite-volume effects checked by comparing
Two volumes compared.}  \\
[1.0ex] \hline \\[-1.2ex]
%
Hasenfratz 08 & \cite{Hasenfratz:2008ce} & 2 & 1.84, 2.77 &  $\epsilon$-regime &  \parbox[t]{4.2cm}{Topology: not fixed, 2 volumes.}\\
[4.0ex] \hline \\[-2.0ex]
%
JLQCD/TWQCD 07  & \cite{Fukaya:2007pn} & 2 & 1.78 & $\epsilon$-regime & \parbox[t]{5.25cm}{Topology: fixed to $\nu=0$.}\\
[1.0ex]\hline \\[-1.2ex]
%
JLQCD/TWQCD 07A & \cite{Aoki:2007pw} & 2 & 1.92 & - & \parbox[t]{5.25cm}{Topology fixed to $\nu=0$ \protect\cite{Aoki:2007ka}.
%gives finite-volume effects estimated through \protect\cite{Aoki:2007ka}.
} \\
[1.0ex] \hline  \\[-1.2ex]
%
CERN-TOV 06 & \cite{DelDebbio:2006cn} & 2 & 1.72, 1.67, 1.88 & 3.5, 3.2, 3.6 & \parbox[t]{5.25cm}{No estimate for FSE.}  \\
[1.0ex] \hline \\[-1.2ex]
%
QCDSF/UKQCD 06A & \cite{Brommel:2006ww} & 2 & 1.4-2.0 & 3.8 & \parbox[t]{4.2cm}{NLO $\chi$PT estimate for FSE \protect\cite{Bunton:2006va}.} \\
[4.0ex] \hline \hline \\[-1.2ex]
\end{tabular*}
\caption{Finite volume effects in $\Nf=2$ determinations of the Low-Energy Constants.}
}
\end{table}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{table}[!htp]
{\footnotesize
\begin{tabular*}{\textwidth}[l]{l @{\extracolsep{\fill}} c c l}
\hline\hline \\[-1.2ex]
Collab. & Ref. & $\Nf$ &  Description
\\[1.0ex] \hline \hline \\[-1.2ex]
%
HPQCD 15B  & \cite{Koponen:2015tkr} & 2+1+1 & ---\\
[1.0ex] \hline \\[-1.2ex]%
%
HPQCD 13A& \cite{Dowdall:2013rya} & 2+1+1 & ---  \\
%
[1.0ex] \hline \\[-1.2ex]%
%
ETM 10,11, 13& \cite{Baron:2010bv, Baron:2011sf, Cichy:2013gja} & 2+1+1 & Non-perturbative\\
%[1.0ex] \hline \\[-1.2ex]%
%
%ETM 11 & \cite{Baron:2011sf} & 2+1+1 & Non-perturbative\\
%[1.0ex] \hline\\[-1.2ex]
%%
%%
%ETM 10 & \cite{Baron:2010bv}  & 2+1+1 & Non-perturbative\\
[1.0ex] \hline \\[-1.2ex]
%
%
JLQCD 16eqs  & \cite{Cossu:2016eqs} & 2+1 & Non-perturbative \\
[1.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 15E & \cite{Boyle:2015exm} & 2+1 & RI-SMOM  \\
[1.0ex] \hline  \\[-1.2ex]
%
JLQCD 15A  & \cite{Aoki:2015pba} & 2+1 & RI-MOM \\
[1.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 14B & \cite{Blum:2014tka} & 2+1 & RI-SMOM \\
[1.0ex] \hline  \\[-1.2ex]
%
JLQCD 14   & \cite{Fukaya:2014jka} & 2+1 & --- \\
[1.0ex] \hline  \\[-1.2ex]
%
Boyle 14 & \cite{Boyle:2014pja} & 2+1 & --- \\
[1.0ex] \hline  \\[-1.2ex]
%
BMW 13 & \cite{Durr:2013goa} & 2+1 & Non-perturbative\\
[1.0ex] \hline  \\[-1.2ex]
%
RBC/UKQCD 12 & \cite{Arthur:2012opa} & 2+1 & Non-perturbative (RI/SMOM)\\
[1.0ex] \hline  \\[-1.2ex]
%
Borsanyi 12 & \cite{Borsanyi:2012zv} & 2+1 & \parbox[t]{7cm}{Indirectly non-perturbative
              through \cite{Durr:2010vn} for $\Sigma$; no renormalization needed for $F$,
              since only $\Fpi/F$ computed and scale set through $\Fpi$.}\\
[7.0ex] \hline  \\[-1.2ex]
%
NPLQCD 11 & \cite{Beane:2011zm} & 2+1 & Not needed (no result for $\Sigma$).\\
[1.0ex] \hline  \\[-1.2ex]
%
JLQCD/TWQCD 10A & \cite{Fukaya:2010na} & 2+1, 3 &  Non-perturbative\\
[1.0ex]\hline \\[-1.2ex]
%
MILC 09, 09A, 10, 10A & \cite{Bazavov:2009bb,Bazavov:2009fk, Bazavov:2010hj, Bazavov:2010yq}  & 2+1 &  2 loop \\
[1.0ex] \hline \\[-1.2ex]
%
RBC/UKQCD 10A & \cite{Aoki:2010dy}   & 2+1 &  Non-perturbative\\
[1.0ex]\hline \\[-1.2ex]
%
JLQCD 09 & \cite{Fukaya:2009fh} & 2+1 & Non-perturbative\\
[1.0ex] \hline \\[-1.2ex]
%
%MILC 09, 09A & \cite{Bazavov:2009bb,Bazavov:2009fk}  & 2+1 &  2 loop \\
%[1.0ex] \hline \\[-1.2ex]
%
TWQCD 08 & \cite{Chiu:2008jq} & 2+1 &  Non-perturbative\\
[1.0ex] \hline \\[-1.2ex]
%
%JLQCD/TWQCD 08B & \cite{Chiu:2008kt} & 2+1 & Non-perturbative\\
%[1.0ex]\hline\\[-1.2ex]
%
PACS-CS 08   & \cite{Aoki:2008sm}     & 2+1 &  1 loop \\
[1.0ex] \hline \\[-1.2ex]
%
RBC/UKQCD 08, 08A & \cite{Allton:2008pn,Boyle:2008yd}   & 2+1 &  Non-perturbative\\
[1.0ex]\hline \\[-1.2ex]
%
NPLQCD 06 & \cite{Beane:2006kx} & 2+1 & --- \\
[1.0ex] \hline \\[-1.2ex]
%
LHP 04 & \cite{Bonnet:2004fr} & 2+1 & --- \\
[1.0ex] \hline \\[-1.2ex]
%
%
 All collaborations  &  & 2 & Non-perturbative\\
[1.0ex] \hline\hline \\[-1.2ex]
%
\end{tabular*}
\caption{Renormalization in determinations of the Low-Energy Constants.}
}
\end{table}

\clearpage


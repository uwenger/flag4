# FLAG plot for BK
# Goes with final version 7/5/13
# run with "python BK7.py"
# SS: filled in green circle for Nf=2 ETMC result
# Goes with final final version 11/30/13
# run with "python BK8.py"
# SS: added SWME 13 and updated average
# UW: This is now called BK.py and produces BK.eps, BK.pdf etc when run 
# with "python BK.py", the version number should not be attached to the file name.
#
# check whether numpy library is present
try: 
 import numpy
except ImportError:
 print "numpy library not found. If you want the FLAG-logo to be added to the"
 print "plot, please install this library"
import numpy as np
################################################################################
# Please edit the following blocks #############################################
################################################################################

# layout specifications
titlestring	= "$g_T$"			# plot title
plotnamestring	= "GT"			# filename for plot
plotxticks	= ([[0.4,0.6,0.8,1.0,1.2]])# locations for x-ticks
nminorticks	= 4			# number of intermediate minor ticks
					# 0 if none
yaxisstrings	= [				# x,y-location for y-strings
		   [+0.44,19.5,"$\\rm N_f=2+1+1$"],
		   [+0.44,13,"$\\rm N_f=2+1$"],
		   [+0.44,7.5,"$\\rm N_f=2$"],
                   [+0.44,2.5,"Pheno."]
		    ]
xlimits		= [0.3,1.9]			# plot's xlimits
logo		= 'upper left'			# either of 'upper right'
					  	#           'upper left'
					  	#           'lower left'
					  	#           'lower right'
tpos = 1.27					# x-position for the data labels
FS = 12	

# the following blocks contain the list of 
# lattice results that will be plotted, one block for 2, 2+1 and 2+1+1, 
# respectively
# 
# column	item
# 0		central value
# 1		position on the y-axis
# 2		neg. error
# 3		pos. error
# 4		Collaboration string
# 5		this column contains layout parameters for the plot-symbol and
#  		and the collaboration text:
#		column	item
#		0 	marker style (see bottom of this file for a full list)
#		1	marker face color r=red, b=blue, k=black, w=white
#					  g=green
#		2	marker color (errorbar and frame), color coding as 
#			for face color
#		3	color intensity 0=full, 1=soso, 2=bleak
dat2p1p1=[
[1.047,0.061,0.061,0.061,0.061,"PNDME 14",['s','w','r',0,tpos]],
[1.020,0.076,0.076,0.076,0.076,"PNDME 15",['s','g','g',0,tpos]],
[0.987,0.055,0.055,0.051,0.051,"PNDME 16",['s','g','g',0,tpos]],
[0.989,0.034,0.034,0.032,0.032,"PNDME 18",['s','g','g',0,tpos]],
]
dat2p1=[
[0.9,0.2,0.2,0.2,0.2,"RBC/UKQCD 10",['s','w','r',0,tpos]],
[1.038,0.016,0.016,0.011,0.011,"LHPC 12",['s','w','r',0,tpos]],
[1.08,0.10,0.10,0.03,0.03,"JLQCD 18",['s','w','r',0,tpos]],
]
dat2=[
[0.93,0.06,0.06,0.06,0.06,"RBC 08",['s','w','r',0,tpos]],
[1.027,0.062,0.062,0.062,0.062,"ETMC 15",['s','w','r',0,tpos]],
[1.005,0.034,0.034,0.017,0.017,"RQCD 14",['s','w','r',0,tpos]],
[1.004,0.028,0.028,0.021,0.021,"ETMC 17",['s','w','r',0,tpos]],
]
pheno=[
[.58,.16,.19,0.0,0.0,"Anselmino 13",['o','b','b',0,tpos]],
[0.66,0.10,0.10,0.0,0.0,"Pitschmann 14",['o','b','b',0,tpos]],
[0.979,0.253,0.253,0.0,0.0,"Goldstein 14",['o','b','b',0,tpos]],
[0.61,0.25,0.15,0.0,0.0,"Kang 15",['o','b','b',0,tpos]],
[0.8,0.45,0.45,0.0,0.0,"Radici 15",['o','b','b',0,tpos]],
]
# The color coding for the FLAG-average (below) is as for the data itself; 
# the additional list at the end is the RGB-code for the errorband.
# Please note that the order inside the list FLAGaverage (if there is more 
# than one) # is significant, the last item will be plotted last and will 
# therefore be plotted on top of all others.

FLAGaverage=[ # there should be as many entries for average as there are data sets
	      # in the case where no average is provided for a given data set
	      # just replace the central value by "NaN"
	      # 
	      # with respect to the above there is another column at the end specifying
	      # the type of FLAG-band
	      # 0 -> dashed lines marking the edge of the interval
	      # 1 -> grey area bounded by solid black lines
[np.nan,0.0,0.0,0.0,0.0,"",['s','k','k',0,tpos],0],
[np.nan,0.03,0.03,0.03,0.03,"FLAG average for $\\rm N_f=2$",['s','k','k',0,tpos],0],
[np.nan,0.03,0.03,0.03,0.03,"FLAG average for $\\rm N_f=2+1$",['s','k','k',0,tpos],0],
[1.0,0.03,0.03,0.03,0.03,"FLAG average for $\\rm N_f=2+1+1$",['s','k','k',0,tpos],0],
]

# The follwing list should contain the list names of the previous DATA-blocks
datasets=[pheno,dat2,dat2p1,dat2p1p1]

################################################################################
# DO NOT EDIT THE FOLLOWING ####################################################
################################################################################


# Now that all the relevant has been specified, generate the plot.
# Please make sure that you have a directory called "plots" in you current 
# working directory. That's where all plots will be stored 
execfile('FLAGplot.py')

# additional documentation:
# '.' 	point marker
# ',' 	pixel marker
# 'o' 	circle marker
# 'v' 	triangle_down marker
# '^' 	triangle_up marker
# '<' 	triangle_left marker
# '>' 	triangle_right marker
# '1' 	tri_down marker
# '2' 	tri_up marker
# '3' 	tri_left marker
# '4' 	tri_right marker
# 's' 	square marker
# 'p' 	pentagon marker
# '*' 	star marker
# 'h' 	hexagon1 marker
# 'H' 	hexagon2 marker
# '+' 	plus marker
# 'x' 	x marker
# 'D' 	diamond marker
# 'd' 	thin_diamond marker
# '|' 	vline marker
# '_' 	hline marker

import re
import os
import numpy as np
import string
import spires

class FLAGresult:
    # CLASS hodling results
    def __init__(self, tag, quantity,reference,Nf,result,section,units ):
        self.tag = tag
        self.quantity = quantity
        self.reference = reference
        self.Nf = Nf
        self.result = result
        self.section = section
        self.units = units

    def Nresults(self):
        if re.findall('cite',self.reference):
         self.res = re.split('{',self.reference)[1]
         self.res = re.split('}',self.res)[0]
         return len(re.split(',',self.res))
 	else:
	 return 'can\'t count Nresults -- no cite'
    
    def printall(self):
        print self.tag
        print self.quantity
        print self.reference 
        print self.Nf
        print self.result
 


def getline(line):
 # reads a line from the macro block now accompanying every FLAG result 
 splitline=re.split('&',line)
 tag=splitline[0]
 tag=re.split('%',tag)[1].replace("\t","").replace(" ","")
 data=[]
 for i in splitline[1:]:
  if (re.match('END\n',i)) or (re.match('END\r\n',i)):
   break
  dum=i.replace(" ","")
  data.append(dum.replace("\t",""))
 return tag,data

def removeNf(dat):
 # the observables in the LEC section contain an explicit N_f which I would like to remove
 dat=re.sub(r"\\big\|_\{(.*?)\}","",dat)
 return dat

def format_error(res): # make sure all results are displayed with the same
		    # style for the error (if only mantissa, remove coma)
 dum = re.split("\(",res)
 serr=''
 err=0.
 for s in dum[1:]:
  ss = re.sub("\)","",s)
  fs = float(ss)
  if (fs<1.):
   serr+="("+str(int(ss[ss.find(".")+1:]))+")"
  else:
   serr+="("+ss+")"
 return dum[0]+serr

def getresult(line):
 # reads a line from the results block (usually the equation containing the result) and
 # extracts the part contained between the delimiters FLAGAVBEGIN and FLAGAVEND
 # the text between the delimiters is expected to be of the form
 # 'variable name' = 'result'
 res=[] #/{(.*?)}/
 if ("|_{\Nf=2" in line):
   line=	 removeNf(line)
 splitline=re.split('FLAGAV',line.replace("&",""))
 if(len(splitline)>1):
  for i in range(len(splitline)):
   if re.match('BEGIN',splitline[i]):
	 dummy=re.split('BEGIN',splitline[i])[1][:-1]
	 #print dummy
	 if not re.search('alpha',dummy):
	  dummy=dummy.replace("\\al","")
	 dummy=dummy.replace("\\!","")
	 res.append(re.split('=',dummy))
	 for j in range(len(res[0])): 
          #res[0][j]=res[0][j].replace(" ","")	      
 	  if re.match("\(",res[0][j]):          # some dimensionful results are quoted like (###\pm#)MeV
	   dummy =  re.sub(r"^\(","",res[0][j]) # the following gets rid of the brackets
	   res[0][j]=dummy.rsplit(')')[-2]
	  if j==1:
 	   if re.search("\\pm",res[0][j]):
 	    res[0][j]=res[0][j].replace("\\;","") # special case where line ends with \;
	    res[0][j]=res[0][j].replace(",","")  # special case where line ends with ,
	    dummy = re.sub(r"\\pm","(",res[0][j])# some results provide error with \pm - this
	    res[0][j]=dummy+")"                  # rewrites results with errors in brackets
	  res[0][j]=re.sub("\~","",res[0][j])
  #print res[0][1],
  res[0][1]=format_error(res[0][1])
 return res

def findtag(allres,tag):
 # searches for a result with a given tag
 found=[]
 for i in range(len(allres)):
  if (allres[i].tag==tag):
    found.append(allres[i])
 return found


def parse_file(filename):
 # parses a FLAG section file for macros and results
 with open(filename) as f:
    FORMULA=[]
    itson=0
    itsonF=0
    allres=[]
    for line in f:
	    if(re.search("\label{sec:",line)):
             current=re.split('label{sec:',line)[1]
             currentsec=re.split('}',current)[0]
	     currentsec = '\\ref{sec:'+currentsec
            elif(re.search("\label{s:",line)):
             current=re.split('label{s:',line)[1]
             currentsec=re.split('}',current)[0]
	     currentsec = '\\ref{s:'+currentsec
            elif(re.search("\label{sec_",line)):
             current=re.split('label{sec_',line)[1]
             currentsec=re.split('}',current)[0]
	     currentsec = '\\ref{sec_'+currentsec
            elif(re.search("\label{subsec",line)):
             current=re.split('label{subsec',line)[1]
             currentsec=re.split('}',current)[0]
	     currentsec = '\\ref{subsec'+currentsec
	    if(re.match("%FLAGRESULTFORMULA BEGIN",line)):
		    results=[]
		    itsonF=1
	    elif(re.match("%FLAGRESULTFORMULA END",line)):
		    itsonF=0
		    for i in range(len(dict0['TAG'])):
	             unit=(dict0['UNITS'][i] if (dict0['UNITS'][i]!='1') else '')
		     #print dict0['TAG'][i],results[i][0][0],dict0['REFS'][i], dict0['FLAVOURs'][i],results[i][0][1],currentsec, unit
                     allres.append(FLAGresult(
			dict0['TAG'][i],results[i][0][0],dict0['REFS'][i],
			dict0['FLAVOURs'][i],results[i][0][1],currentsec, unit))
	    elif(re.match("%FLAGRESULT BEGIN",line)):
    	            dict0={}
		    itson=1
            elif(re.match("%FLAGRESULT END",line)):
                    itson=0
            elif(itson==1):
             tag,items = getline(line)
             dict0[tag]=items
            elif(itsonF==1):
             FORMULA.append(line)
	     dummy = getresult(line)
	     if dummy:
 	      results.append(dummy)
 return allres,FORMULA

################################################################################
## START ACTUAL WORK OF COMPILING ALL RESULTS ##################################
################################################################################
# TABLE TWO
################################################################################
# remove old bibtex files
os.system('rm ../web/*.bib')
filenames=["../HQ/HQSubsections/DL.tex",
	   "../HQ/HQSubsections/DSL.tex",
	   "../HQ/HQSubsections/BL.tex",
	   "../HQ/HQSubsections/BB.tex"]
#	   "../Alpha_s/s9.tex"]
FF=[]
for i in filenames:
 FF.append(get_all_in_file(i))
get_all_in_file("../Alpha_s/s9.tex")

################################################################################
# TABLE ONE
################################################################################
filenames=["../qmass/qmass_mudms.tex",
	   "../qmass/qmass_mc.tex",
	   "../qmass/qmass_mb.tex",
	   "../Vudus/VudVus.tex",
	   "../LEC/LECs.tex",
	   "../BK/BK.tex"]

FF=[]
for i in filenames:
 FF.append(get_all_in_file(i))

import numpy as np
import spires
import re
# prints missing bibtex entries as of logfile specified in "path" to terminal
path='../FLAG_master.log'
missings=[]
for line in open(path, 'r'):
   match_eprint = re.compile(r'Citation')
   if match_eprint.findall(line):
    eprint=re.findall('\`.*\'',line)[0][1:-1]
    rType, ref = spires.findRefType(eprint)
    missings.append(ref)
    #print spires.getBiBTeX(ref,rType)
for ref in list(set(missings)): # set gets list of unique entries
    print spires.getBiBTeX(ref,rType)
